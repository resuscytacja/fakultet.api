﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace fakultet.DAL.Migrations
{
    public partial class GOD_MAKE_IT_WORK_PLZ : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "JobPosition",
                table: "AspNetUsers",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "DocumentTypes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    AcceptingUserAccount = table.Column<int>(nullable: false),
                    VerifyingUserAccount = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DocumentTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Documents",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DocumentTypeId = table.Column<int>(nullable: true),
                    Status = table.Column<int>(nullable: false),
                    Title = table.Column<string>(nullable: true),
                    SendTime = table.Column<DateTime>(nullable: false),
                    RejectedReason = table.Column<string>(nullable: true),
                    CancelledReason = table.Column<string>(nullable: true),
                    ConcelledById = table.Column<string>(nullable: true),
                    RejectedById = table.Column<string>(nullable: true),
                    ResolveTime = table.Column<DateTime>(nullable: true),
                    CreatedById = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Documents", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Documents_AspNetUsers_ConcelledById",
                        column: x => x.ConcelledById,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Documents_AspNetUsers_CreatedById",
                        column: x => x.CreatedById,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Documents_DocumentTypes_DocumentTypeId",
                        column: x => x.DocumentTypeId,
                        principalTable: "DocumentTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Documents_AspNetUsers_RejectedById",
                        column: x => x.RejectedById,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "DocumentActivities",
                columns: table => new
                {
                    DocumentActivityId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Type = table.Column<int>(nullable: false),
                    DocumentId = table.Column<int>(nullable: true),
                    Message = table.Column<string>(nullable: true),
                    InitiatedById = table.Column<string>(nullable: true),
                    DateTime = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DocumentActivities", x => x.DocumentActivityId);
                    table.ForeignKey(
                        name: "FK_DocumentActivities_Documents_DocumentId",
                        column: x => x.DocumentId,
                        principalTable: "Documents",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DocumentActivities_AspNetUsers_InitiatedById",
                        column: x => x.InitiatedById,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Files",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    FileName = table.Column<string>(nullable: true),
                    DocumentId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Files", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Files_Documents_DocumentId",
                        column: x => x.DocumentId,
                        principalTable: "Documents",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { "a18332c0-666b-4af8-bd17-00bd9344e575", "704ce039-8536-4727-9931-4004b21872f2", "Student", "STUDENT" },
                    { "a18332c0-420b-4af8-bd17-00bd9344e575", "aef5fb62-22bd-4d68-b0a5-ee68952a2065", "Superadmin", "SUPERADMIN" },
                    { "a18332c0-admi-naf8-bd17-00bd9344e575", "93656f21-8248-488c-96bd-8ac5a722db59", "Admin", "ADMIN" },
                    { "a18332c0-mode-rato-rd17-00bd9344e575", "f5c75649-18d5-488d-a6d6-42553473863e", "Moderator", "MODERATOR" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "Email", "EmailConfirmed", "FirstName", "JobPosition", "LastName", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "NumerIndeksu", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UserName" },
                values: new object[,]
                {
                    { "bdziek4n-admi-naf8-bd17-00bd9344e575", 0, "c7a547e8-e4cc-41e0-9038-e118172a4b03", "dziekan@fakultet.pl", true, "Jakub", 2, "Marzyciel", false, null, "DZIEKAN@FAKULTET.PL", "DZIEKAN@FAKULTET.PL", null, "AQAAAAEAACcQAAAAELew+jtaghdaPbU5DVRNAbEGRpgYaMJhxMss6+KGmG+dNdhed4sJOTzGPczefkzsJA==", null, false, "", false, "dziekan@fakultet.pl" },
                    { "b01rektor-admi-naf8-bd17-00bd9344e575", 0, "45d84db6-00d2-46a2-8f41-1e179039a068", "rektor@fakultet.pl", true, "Michał", 3, "Szczytny", false, null, "REKTOR@FAKULTET.PL", "REKTOR@FAKULTET.PL", null, "AQAAAAEAACcQAAAAEMDAKNUqyRLkHwGFTk7B0brvl5fh8Hxq2jF7z6D0q26ebIvOp4FCgTv4Q9TuDRjLDg==", null, false, "", false, "rektor@fakultet.pl" },
                    { "b01bex90-admi-naf8-bd17-00bd9344e575", 0, "24a595e5-6330-46e5-86fd-52c07ba84c08", "admin@fakultet.pl", true, "Pan", 0, "SuperAdmin", false, null, "ADMIN@FAKULTET.pl", "SUPERADMIN123", null, "AQAAAAEAACcQAAAAELlu1bbyfFaAr5MDFvtkadeHpgKZxJfMV/bLnwn7FF35k5NzwNmRoxB7bSuiKb1ymw==", null, false, "", false, "superadmin123" },
                    { "bdziek4n-admi-naf8-bd17-pracownik4e575", 0, "91de5fed-11b4-4b5e-8729-2372ada27284", "pracownikdziekanatu@fakultet.pl", true, "Patryk", 1, "Kyrtap", false, null, "PRACOWNIKDZIEKANATU@FAKULTET.PL", "PRACOWNIKDZIEKANATU@FAKULTET.PL", null, "AQAAAAEAACcQAAAAEH2I1vLUEm/E+mTaWoLYN7FGZBPccRoGVckntYxaFBv8SVNZ3mkGSwMyB211yKq8hg==", null, false, "", false, "pracownikdziekanatu@fakultet.pl" },
                    { "b01bex90-stud-ent8-bd17-00bd9344e575", 0, "12069939-98be-4b73-b58d-7976d3fe51bd", "student@fakultet.pl", true, "Pilny", 0, "Student", false, null, "STUDENT@FAKULTET.pl", "STUDENT@FAKULTET.pl", "123456", "AQAAAAEAACcQAAAAELl5CdIAEay5phpfYUnfx+TR2ul+wVKiHiojac23+Sflrf/31TQaDoUwOKNE8GT6FQ==", null, false, "", false, "student@fakultet.pl" }
                });

            migrationBuilder.InsertData(
                table: "DocumentTypes",
                columns: new[] { "Id", "AcceptingUserAccount", "Name", "VerifyingUserAccount" },
                values: new object[,]
                {
                    { 6, 3, "Zmiana kierunku studiów", 1 },
                    { 7, 3, "Zmiana terminu pracy dyplomowej", 1 },
                    { 8, 3, "Ponowne powtarzanie semestru", 1 },
                    { 3, 2, "Urlop dziekański", 1 },
                    { 2, 2, "Warunki", 1 },
                    { 1, 2, "Stypendia", 1 },
                    { 4, 2, "Podanie o dyplom", 1 },
                    { 5, 2, "Wypis z uczelni", 1 }
                });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "UserId", "RoleId" },
                values: new object[,]
                {
                    { "b01bex90-admi-naf8-bd17-00bd9344e575", "a18332c0-420b-4af8-bd17-00bd9344e575" },
                    { "b01rektor-admi-naf8-bd17-00bd9344e575", "a18332c0-admi-naf8-bd17-00bd9344e575" },
                    { "bdziek4n-admi-naf8-bd17-00bd9344e575", "a18332c0-admi-naf8-bd17-00bd9344e575" },
                    { "bdziek4n-admi-naf8-bd17-pracownik4e575", "a18332c0-mode-rato-rd17-00bd9344e575" },
                    { "b01bex90-stud-ent8-bd17-00bd9344e575", "a18332c0-666b-4af8-bd17-00bd9344e575" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_DocumentActivities_DocumentId",
                table: "DocumentActivities",
                column: "DocumentId");

            migrationBuilder.CreateIndex(
                name: "IX_DocumentActivities_InitiatedById",
                table: "DocumentActivities",
                column: "InitiatedById");

            migrationBuilder.CreateIndex(
                name: "IX_Documents_ConcelledById",
                table: "Documents",
                column: "ConcelledById");

            migrationBuilder.CreateIndex(
                name: "IX_Documents_CreatedById",
                table: "Documents",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_Documents_DocumentTypeId",
                table: "Documents",
                column: "DocumentTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Documents_RejectedById",
                table: "Documents",
                column: "RejectedById");

            migrationBuilder.CreateIndex(
                name: "IX_Files_DocumentId",
                table: "Files",
                column: "DocumentId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DocumentActivities");

            migrationBuilder.DropTable(
                name: "Files");

            migrationBuilder.DropTable(
                name: "Documents");

            migrationBuilder.DropTable(
                name: "DocumentTypes");

            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "UserId", "RoleId" },
                keyValues: new object[] { "b01bex90-admi-naf8-bd17-00bd9344e575", "a18332c0-420b-4af8-bd17-00bd9344e575" });

            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "UserId", "RoleId" },
                keyValues: new object[] { "b01bex90-stud-ent8-bd17-00bd9344e575", "a18332c0-666b-4af8-bd17-00bd9344e575" });

            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "UserId", "RoleId" },
                keyValues: new object[] { "b01rektor-admi-naf8-bd17-00bd9344e575", "a18332c0-admi-naf8-bd17-00bd9344e575" });

            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "UserId", "RoleId" },
                keyValues: new object[] { "bdziek4n-admi-naf8-bd17-00bd9344e575", "a18332c0-admi-naf8-bd17-00bd9344e575" });

            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "UserId", "RoleId" },
                keyValues: new object[] { "bdziek4n-admi-naf8-bd17-pracownik4e575", "a18332c0-mode-rato-rd17-00bd9344e575" });

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18332c0-420b-4af8-bd17-00bd9344e575");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18332c0-666b-4af8-bd17-00bd9344e575");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18332c0-admi-naf8-bd17-00bd9344e575");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18332c0-mode-rato-rd17-00bd9344e575");

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "b01bex90-admi-naf8-bd17-00bd9344e575");

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "b01bex90-stud-ent8-bd17-00bd9344e575");

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "b01rektor-admi-naf8-bd17-00bd9344e575");

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "bdziek4n-admi-naf8-bd17-00bd9344e575");

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "bdziek4n-admi-naf8-bd17-pracownik4e575");

            migrationBuilder.DropColumn(
                name: "JobPosition",
                table: "AspNetUsers");
        }
    }
}
