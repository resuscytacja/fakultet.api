﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace fakultet.DAL.Migrations
{
    public partial class updateModelDocument : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Documents_AspNetUsers_RejectedById",
                table: "Documents");

            migrationBuilder.RenameColumn(
                name: "RejectedReason",
                table: "Documents",
                newName: "UnverifiedReason");

            migrationBuilder.RenameColumn(
                name: "RejectedById",
                table: "Documents",
                newName: "UnverifiedById");

            migrationBuilder.RenameIndex(
                name: "IX_Documents_RejectedById",
                table: "Documents",
                newName: "IX_Documents_UnverifiedById");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18332c0-420b-4af8-bd17-00bd9344e575",
                column: "ConcurrencyStamp",
                value: "c88bed00-e192-4aa3-aa67-0ef4b529d04f");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18332c0-666b-4af8-bd17-00bd9344e575",
                column: "ConcurrencyStamp",
                value: "32a00378-0627-48d1-adf8-1b78d988ade9");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18332c0-admi-naf8-bd17-00bd9344e575",
                column: "ConcurrencyStamp",
                value: "d99dbfab-d2d6-42d4-96e8-ecd19b64d7f4");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18332c0-mode-rato-rd17-00bd9344e575",
                column: "ConcurrencyStamp",
                value: "552ab0ef-dea3-4b71-9c18-d4e6ca23c95c");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "b01bex90-admi-naf8-bd17-00bd9344e575",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "af3ea060-6841-4061-ad22-c380a917131f", "AQAAAAEAACcQAAAAECcvvG9oFmbbkzM67AciTGcyQua+bQGXQcKCQOxIFfVbqhwM8tXMDCmHjT3Yu57Baw==" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "b01bex90-stud-ent8-bd17-00bd9344e575",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "95fd4aa6-bcc5-4f7f-9b90-9df6cac49a3a", "AQAAAAEAACcQAAAAEEumGwn/qaQ8r2PnvDTU569xWK6W/2r7zRZPlvCymi0mFaXheXKYejwD/nbGFKcY1A==" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "b01rektor-admi-naf8-bd17-00bd9344e575",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "7f1dd850-417b-41f0-ad24-d2722b8d4ec5", "AQAAAAEAACcQAAAAEFkR2v9sDaonyM5x31t0i/ZG2/eJl5EaOmweWKlUp9GfYjDruIijG2VntgAKDoT1TA==" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "bdziek4n-admi-naf8-bd17-00bd9344e575",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "186d5951-b75b-4fa8-a95e-b24daf1a850b", "AQAAAAEAACcQAAAAEJsOCGTxMQFLir3pPquTKoMdKiO135gRREqmurvk3DSUna/zJyC1TQFxtaeCsyQ0ew==" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "bdziek4n-admi-naf8-bd17-pracownik4e575",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "0d7ca44f-5700-4f51-9470-313b726ee1d4", "AQAAAAEAACcQAAAAEOIoDLqlBuGcyuchFfHsGUPKjC268rIc0i96bckd/aS2aknEw6vpIMpmTkAg/AhOXw==" });

            migrationBuilder.AddForeignKey(
                name: "FK_Documents_AspNetUsers_UnverifiedById",
                table: "Documents",
                column: "UnverifiedById",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Documents_AspNetUsers_UnverifiedById",
                table: "Documents");

            migrationBuilder.RenameColumn(
                name: "UnverifiedReason",
                table: "Documents",
                newName: "RejectedReason");

            migrationBuilder.RenameColumn(
                name: "UnverifiedById",
                table: "Documents",
                newName: "RejectedById");

            migrationBuilder.RenameIndex(
                name: "IX_Documents_UnverifiedById",
                table: "Documents",
                newName: "IX_Documents_RejectedById");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18332c0-420b-4af8-bd17-00bd9344e575",
                column: "ConcurrencyStamp",
                value: "aef5fb62-22bd-4d68-b0a5-ee68952a2065");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18332c0-666b-4af8-bd17-00bd9344e575",
                column: "ConcurrencyStamp",
                value: "704ce039-8536-4727-9931-4004b21872f2");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18332c0-admi-naf8-bd17-00bd9344e575",
                column: "ConcurrencyStamp",
                value: "93656f21-8248-488c-96bd-8ac5a722db59");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18332c0-mode-rato-rd17-00bd9344e575",
                column: "ConcurrencyStamp",
                value: "f5c75649-18d5-488d-a6d6-42553473863e");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "b01bex90-admi-naf8-bd17-00bd9344e575",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "24a595e5-6330-46e5-86fd-52c07ba84c08", "AQAAAAEAACcQAAAAELlu1bbyfFaAr5MDFvtkadeHpgKZxJfMV/bLnwn7FF35k5NzwNmRoxB7bSuiKb1ymw==" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "b01bex90-stud-ent8-bd17-00bd9344e575",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "12069939-98be-4b73-b58d-7976d3fe51bd", "AQAAAAEAACcQAAAAELl5CdIAEay5phpfYUnfx+TR2ul+wVKiHiojac23+Sflrf/31TQaDoUwOKNE8GT6FQ==" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "b01rektor-admi-naf8-bd17-00bd9344e575",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "45d84db6-00d2-46a2-8f41-1e179039a068", "AQAAAAEAACcQAAAAEMDAKNUqyRLkHwGFTk7B0brvl5fh8Hxq2jF7z6D0q26ebIvOp4FCgTv4Q9TuDRjLDg==" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "bdziek4n-admi-naf8-bd17-00bd9344e575",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "c7a547e8-e4cc-41e0-9038-e118172a4b03", "AQAAAAEAACcQAAAAELew+jtaghdaPbU5DVRNAbEGRpgYaMJhxMss6+KGmG+dNdhed4sJOTzGPczefkzsJA==" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "bdziek4n-admi-naf8-bd17-pracownik4e575",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "91de5fed-11b4-4b5e-8729-2372ada27284", "AQAAAAEAACcQAAAAEH2I1vLUEm/E+mTaWoLYN7FGZBPccRoGVckntYxaFBv8SVNZ3mkGSwMyB211yKq8hg==" });

            migrationBuilder.AddForeignKey(
                name: "FK_Documents_AspNetUsers_RejectedById",
                table: "Documents",
                column: "RejectedById",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
