﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using fakultet.DAL;

namespace fakultet.DAL.Migrations
{
    [DbContext(typeof(AppDbContext))]
    [Migration("20190602201425_NoneToStudent")]
    partial class NoneToStudent
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.2.4-servicing-10062")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityRole", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("Name")
                        .HasMaxLength(256);

                    b.Property<string>("NormalizedName")
                        .HasMaxLength(256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedName")
                        .IsUnique()
                        .HasName("RoleNameIndex")
                        .HasFilter("[NormalizedName] IS NOT NULL");

                    b.ToTable("AspNetRoles");

                    b.HasData(
                        new
                        {
                            Id = "a18332c0-666b-4af8-bd17-00bd9344e575",
                            ConcurrencyStamp = "99821a72-084a-4519-a81c-c8ee71c96229",
                            Name = "Student",
                            NormalizedName = "STUDENT"
                        },
                        new
                        {
                            Id = "a18332c0-420b-4af8-bd17-00bd9344e575",
                            ConcurrencyStamp = "20f847be-92a7-4b46-9f33-c1d8025d3e1f",
                            Name = "Superadmin",
                            NormalizedName = "SUPERADMIN"
                        },
                        new
                        {
                            Id = "a18332c0-admi-naf8-bd17-00bd9344e575",
                            ConcurrencyStamp = "62bef42e-ef72-4cfa-a609-50476d6e08da",
                            Name = "Admin",
                            NormalizedName = "ADMIN"
                        },
                        new
                        {
                            Id = "a18332c0-mode-rato-rd17-00bd9344e575",
                            ConcurrencyStamp = "f3a9cf01-86d9-44d1-8c5b-89e7a45cbb05",
                            Name = "Moderator",
                            NormalizedName = "MODERATOR"
                        });
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityRoleClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<string>("RoleId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetRoleClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<string>("UserId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserLogin<string>", b =>
                {
                    b.Property<string>("LoginProvider");

                    b.Property<string>("ProviderKey");

                    b.Property<string>("ProviderDisplayName");

                    b.Property<string>("UserId")
                        .IsRequired();

                    b.HasKey("LoginProvider", "ProviderKey");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserLogins");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserRole<string>", b =>
                {
                    b.Property<string>("UserId");

                    b.Property<string>("RoleId");

                    b.HasKey("UserId", "RoleId");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetUserRoles");

                    b.HasData(
                        new
                        {
                            UserId = "b01bex90-stud-ent8-bd17-00bd9344e575",
                            RoleId = "a18332c0-666b-4af8-bd17-00bd9344e575"
                        },
                        new
                        {
                            UserId = "b01bex90-admi-naf8-bd17-00bd9344e575",
                            RoleId = "a18332c0-420b-4af8-bd17-00bd9344e575"
                        },
                        new
                        {
                            UserId = "b01rektor-admi-naf8-bd17-00bd9344e575",
                            RoleId = "a18332c0-admi-naf8-bd17-00bd9344e575"
                        },
                        new
                        {
                            UserId = "bdziek4n-admi-naf8-bd17-00bd9344e575",
                            RoleId = "a18332c0-admi-naf8-bd17-00bd9344e575"
                        },
                        new
                        {
                            UserId = "bdziek4n-admi-naf8-bd17-pracownik4e575",
                            RoleId = "a18332c0-mode-rato-rd17-00bd9344e575"
                        });
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserToken<string>", b =>
                {
                    b.Property<string>("UserId");

                    b.Property<string>("LoginProvider");

                    b.Property<string>("Name");

                    b.Property<string>("Value");

                    b.HasKey("UserId", "LoginProvider", "Name");

                    b.ToTable("AspNetUserTokens");
                });

            modelBuilder.Entity("fakultet.DAL.Models.Document", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("CancelledReason");

                    b.Property<string>("ConsiderationById");

                    b.Property<DateTime?>("ConsiderationTime");

                    b.Property<string>("CreatedById");

                    b.Property<int?>("DocumentTypeId");

                    b.Property<string>("ReturnedReason");

                    b.Property<DateTime>("SendTime");

                    b.Property<int>("Status");

                    b.Property<string>("Title");

                    b.Property<string>("UnverifiedReason");

                    b.Property<string>("VerificationById");

                    b.Property<DateTime?>("VerificationTime");

                    b.HasKey("Id");

                    b.HasIndex("ConsiderationById");

                    b.HasIndex("CreatedById");

                    b.HasIndex("DocumentTypeId");

                    b.HasIndex("VerificationById");

                    b.ToTable("Documents");
                });

            modelBuilder.Entity("fakultet.DAL.Models.DocumentActivity", b =>
                {
                    b.Property<int>("DocumentActivityId")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime>("DateTime");

                    b.Property<int>("DocumentId");

                    b.Property<string>("InitiatedById");

                    b.Property<string>("Message");

                    b.Property<int>("Type");

                    b.HasKey("DocumentActivityId");

                    b.HasIndex("DocumentId");

                    b.HasIndex("InitiatedById");

                    b.ToTable("DocumentActivities");
                });

            modelBuilder.Entity("fakultet.DAL.Models.DocumentType", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("AcceptingUserAccount");

                    b.Property<int>("AllowedToSendUserAccount");

                    b.Property<string>("Name");

                    b.Property<int>("VerifyingUserAccount");

                    b.HasKey("Id");

                    b.ToTable("DocumentTypes");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            AcceptingUserAccount = 2,
                            AllowedToSendUserAccount = 0,
                            Name = "Stypendia",
                            VerifyingUserAccount = 1
                        },
                        new
                        {
                            Id = 2,
                            AcceptingUserAccount = 2,
                            AllowedToSendUserAccount = 0,
                            Name = "Warunki",
                            VerifyingUserAccount = 1
                        },
                        new
                        {
                            Id = 3,
                            AcceptingUserAccount = 2,
                            AllowedToSendUserAccount = 0,
                            Name = "Urlop dziekański",
                            VerifyingUserAccount = 1
                        },
                        new
                        {
                            Id = 4,
                            AcceptingUserAccount = 2,
                            AllowedToSendUserAccount = 0,
                            Name = "Podanie o dyplom",
                            VerifyingUserAccount = 1
                        },
                        new
                        {
                            Id = 5,
                            AcceptingUserAccount = 2,
                            AllowedToSendUserAccount = 0,
                            Name = "Wypis z uczelni",
                            VerifyingUserAccount = 1
                        },
                        new
                        {
                            Id = 6,
                            AcceptingUserAccount = 3,
                            AllowedToSendUserAccount = 0,
                            Name = "Zmiana kierunku studiów",
                            VerifyingUserAccount = 1
                        },
                        new
                        {
                            Id = 7,
                            AcceptingUserAccount = 3,
                            AllowedToSendUserAccount = 0,
                            Name = "Zmiana terminu pracy dyplomowej",
                            VerifyingUserAccount = 1
                        },
                        new
                        {
                            Id = 8,
                            AcceptingUserAccount = 3,
                            AllowedToSendUserAccount = 0,
                            Name = "Ponowne powtarzanie semestru",
                            VerifyingUserAccount = 1
                        });
                });

            modelBuilder.Entity("fakultet.DAL.Models.Files", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int?>("DocumentId");

                    b.Property<string>("FileName");

                    b.HasKey("Id");

                    b.HasIndex("DocumentId");

                    b.ToTable("Files");
                });

            modelBuilder.Entity("fakultet.DAL.Models.UserAccount", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("AccessFailedCount");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("Email")
                        .HasMaxLength(256);

                    b.Property<bool>("EmailConfirmed");

                    b.Property<string>("FirstName");

                    b.Property<int>("JobPosition");

                    b.Property<string>("LastName");

                    b.Property<bool>("LockoutEnabled");

                    b.Property<DateTimeOffset?>("LockoutEnd");

                    b.Property<string>("NormalizedEmail")
                        .HasMaxLength(256);

                    b.Property<string>("NormalizedUserName")
                        .HasMaxLength(256);

                    b.Property<string>("NumerIndeksu");

                    b.Property<string>("PasswordHash");

                    b.Property<string>("PhoneNumber");

                    b.Property<bool>("PhoneNumberConfirmed");

                    b.Property<string>("SecurityStamp");

                    b.Property<bool>("TwoFactorEnabled");

                    b.Property<string>("UserName")
                        .HasMaxLength(256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedEmail")
                        .HasName("EmailIndex");

                    b.HasIndex("NormalizedUserName")
                        .IsUnique()
                        .HasName("UserNameIndex")
                        .HasFilter("[NormalizedUserName] IS NOT NULL");

                    b.ToTable("AspNetUsers");

                    b.HasData(
                        new
                        {
                            Id = "b01bex90-admi-naf8-bd17-00bd9344e575",
                            AccessFailedCount = 0,
                            ConcurrencyStamp = "9785f8b5-48a1-4783-bcf4-655dc3ea64cc",
                            Email = "admin@fakultet.pl",
                            EmailConfirmed = true,
                            FirstName = "Pan",
                            JobPosition = 4,
                            LastName = "SuperAdmin",
                            LockoutEnabled = false,
                            NormalizedEmail = "ADMIN@FAKULTET.pl",
                            NormalizedUserName = "SUPERADMIN123",
                            PasswordHash = "AQAAAAEAACcQAAAAEKEaf5r/lp75XQEgZohRT5C1ZOUvbTVfuJ4mc75D5u/zBc7vQEJDI1ZbBZe+0NQSfQ==",
                            PhoneNumberConfirmed = false,
                            SecurityStamp = "",
                            TwoFactorEnabled = false,
                            UserName = "superadmin123"
                        },
                        new
                        {
                            Id = "b01rektor-admi-naf8-bd17-00bd9344e575",
                            AccessFailedCount = 0,
                            ConcurrencyStamp = "6c06f8a4-508b-45e6-99f0-1de81b5d0d68",
                            Email = "rektor@fakultet.pl",
                            EmailConfirmed = true,
                            FirstName = "Michał",
                            JobPosition = 3,
                            LastName = "Szczytny",
                            LockoutEnabled = false,
                            NormalizedEmail = "REKTOR@FAKULTET.PL",
                            NormalizedUserName = "REKTOR@FAKULTET.PL",
                            PasswordHash = "AQAAAAEAACcQAAAAEPgtrU+S5CsIrfqPg7Wxz2FiW2jD2Tk9SdQm4OcBrpJfm8d6AfQtpXkxOcjIUSClrg==",
                            PhoneNumberConfirmed = false,
                            SecurityStamp = "",
                            TwoFactorEnabled = false,
                            UserName = "rektor@fakultet.pl"
                        },
                        new
                        {
                            Id = "bdziek4n-admi-naf8-bd17-00bd9344e575",
                            AccessFailedCount = 0,
                            ConcurrencyStamp = "924b9fef-919b-4611-b600-44aeb4d8507c",
                            Email = "dziekan@fakultet.pl",
                            EmailConfirmed = true,
                            FirstName = "Jakub",
                            JobPosition = 2,
                            LastName = "Marzyciel",
                            LockoutEnabled = false,
                            NormalizedEmail = "DZIEKAN@FAKULTET.PL",
                            NormalizedUserName = "DZIEKAN@FAKULTET.PL",
                            PasswordHash = "AQAAAAEAACcQAAAAEMEJPxrbF1cj40ddUvulYiT2dlhH43/xZa4DY/N0ZTZcVLhtXRI5oWTsIUJmAbYDCQ==",
                            PhoneNumberConfirmed = false,
                            SecurityStamp = "",
                            TwoFactorEnabled = false,
                            UserName = "dziekan@fakultet.pl"
                        },
                        new
                        {
                            Id = "bdziek4n-admi-naf8-bd17-pracownik4e575",
                            AccessFailedCount = 0,
                            ConcurrencyStamp = "b3867442-0ff2-4c23-ae21-b28ab4b7d0c6",
                            Email = "pracownikdziekanatu@fakultet.pl",
                            EmailConfirmed = true,
                            FirstName = "Patryk",
                            JobPosition = 1,
                            LastName = "Kyrtap",
                            LockoutEnabled = false,
                            NormalizedEmail = "PRACOWNIKDZIEKANATU@FAKULTET.PL",
                            NormalizedUserName = "PRACOWNIKDZIEKANATU@FAKULTET.PL",
                            PasswordHash = "AQAAAAEAACcQAAAAEN8/dwff88tAC7hnuxb1Dg0xZsFHkW9L6eieGyugaUeQaSxGwxERtK/UO5JJbTIw3Q==",
                            PhoneNumberConfirmed = false,
                            SecurityStamp = "",
                            TwoFactorEnabled = false,
                            UserName = "pracownikdziekanatu@fakultet.pl"
                        },
                        new
                        {
                            Id = "b01bex90-stud-ent8-bd17-00bd9344e575",
                            AccessFailedCount = 0,
                            ConcurrencyStamp = "71e28b9b-f739-4130-9a85-1851c8e7f15b",
                            Email = "student@fakultet.pl",
                            EmailConfirmed = true,
                            FirstName = "Pilny",
                            JobPosition = 0,
                            LastName = "Student",
                            LockoutEnabled = false,
                            NormalizedEmail = "STUDENT@FAKULTET.pl",
                            NormalizedUserName = "STUDENT@FAKULTET.pl",
                            NumerIndeksu = "123456",
                            PasswordHash = "AQAAAAEAACcQAAAAEA9iTrxNwOZi6spN2T3Tb5xDOUIWbIHfSHIaIJrCqx7F9zC4PA9ntXJgnkynoofT3w==",
                            PhoneNumberConfirmed = false,
                            SecurityStamp = "",
                            TwoFactorEnabled = false,
                            UserName = "student@fakultet.pl"
                        });
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityRoleClaim<string>", b =>
                {
                    b.HasOne("Microsoft.AspNetCore.Identity.IdentityRole")
                        .WithMany()
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserClaim<string>", b =>
                {
                    b.HasOne("fakultet.DAL.Models.UserAccount")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserLogin<string>", b =>
                {
                    b.HasOne("fakultet.DAL.Models.UserAccount")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserRole<string>", b =>
                {
                    b.HasOne("Microsoft.AspNetCore.Identity.IdentityRole")
                        .WithMany()
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("fakultet.DAL.Models.UserAccount")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserToken<string>", b =>
                {
                    b.HasOne("fakultet.DAL.Models.UserAccount")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("fakultet.DAL.Models.Document", b =>
                {
                    b.HasOne("fakultet.DAL.Models.UserAccount", "ConsiderationBy")
                        .WithMany()
                        .HasForeignKey("ConsiderationById");

                    b.HasOne("fakultet.DAL.Models.UserAccount", "CreatedBy")
                        .WithMany()
                        .HasForeignKey("CreatedById");

                    b.HasOne("fakultet.DAL.Models.DocumentType", "DocumentType")
                        .WithMany()
                        .HasForeignKey("DocumentTypeId");

                    b.HasOne("fakultet.DAL.Models.UserAccount", "VerificationBy")
                        .WithMany()
                        .HasForeignKey("VerificationById");
                });

            modelBuilder.Entity("fakultet.DAL.Models.DocumentActivity", b =>
                {
                    b.HasOne("fakultet.DAL.Models.Document", "Document")
                        .WithMany("Activities")
                        .HasForeignKey("DocumentId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("fakultet.DAL.Models.UserAccount", "InitiatedBy")
                        .WithMany()
                        .HasForeignKey("InitiatedById");
                });

            modelBuilder.Entity("fakultet.DAL.Models.Files", b =>
                {
                    b.HasOne("fakultet.DAL.Models.Document", "Document")
                        .WithMany("FileList")
                        .HasForeignKey("DocumentId");
                });
#pragma warning restore 612, 618
        }
    }
}
