﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace fakultet.DAL.Migrations
{
    public partial class PoleDocumentActivitiesWDocument : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DocumentActivities_Documents_DocumentId",
                table: "DocumentActivities");

            migrationBuilder.AlterColumn<int>(
                name: "DocumentId",
                table: "DocumentActivities",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18332c0-420b-4af8-bd17-00bd9344e575",
                column: "ConcurrencyStamp",
                value: "5193720c-24c3-4941-8b3b-1a0b72246cf3");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18332c0-666b-4af8-bd17-00bd9344e575",
                column: "ConcurrencyStamp",
                value: "19c9ecf6-0b8e-42d0-9ec0-d4751171c523");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18332c0-admi-naf8-bd17-00bd9344e575",
                column: "ConcurrencyStamp",
                value: "efce5206-8dcb-479a-92ac-163da3058c8c");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18332c0-mode-rato-rd17-00bd9344e575",
                column: "ConcurrencyStamp",
                value: "eae59da8-8f7b-437d-a662-9a0b43ac16ea");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "b01bex90-admi-naf8-bd17-00bd9344e575",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "5f5a0ea0-c5f9-4650-a553-79566c2a840a", "AQAAAAEAACcQAAAAEEo6EzHYcPr3Ul+fClFKskpIjeF/Oa+wCefwVuC3uCIZ/eGEuIOdtw5rx7TIePQ/NQ==" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "b01bex90-stud-ent8-bd17-00bd9344e575",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "190cf241-5250-4c6d-8cde-fe7bc13b0cef", "AQAAAAEAACcQAAAAEIqcFbc9JzhE3rABoyn2AfXzqvkhBSS242Ebt7idrTioqBw+nFom+OftSqa/pGZiMQ==" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "b01rektor-admi-naf8-bd17-00bd9344e575",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "e7ed8806-c61a-48ba-8e20-31055fb0faeb", "AQAAAAEAACcQAAAAEDjzJA0VHJBnZmQyk5GRH4imRwD0XpXlSg32n5u6d7ogE+shmQebq7sFxUft3qzqAQ==" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "bdziek4n-admi-naf8-bd17-00bd9344e575",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "808c84f9-fa41-4b39-8ead-caed514798df", "AQAAAAEAACcQAAAAEM3Xs7xSW4RUwqr3ITidlPrxUurFh15BJ7ga93eBybEfh7bS7BdiZMdnpKyTSM7Gsg==" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "bdziek4n-admi-naf8-bd17-pracownik4e575",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "73535c35-4b7d-48df-b3bb-6d8fdf1f1476", "AQAAAAEAACcQAAAAEI+UjMZeZYh+f6pee8o37GbLSaWcRbthrSiq5PHY3dwVv2RmcCMOZFKDfQvRBN2ZRQ==" });

            migrationBuilder.AddForeignKey(
                name: "FK_DocumentActivities_Documents_DocumentId",
                table: "DocumentActivities",
                column: "DocumentId",
                principalTable: "Documents",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DocumentActivities_Documents_DocumentId",
                table: "DocumentActivities");

            migrationBuilder.AlterColumn<int>(
                name: "DocumentId",
                table: "DocumentActivities",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18332c0-420b-4af8-bd17-00bd9344e575",
                column: "ConcurrencyStamp",
                value: "c88bed00-e192-4aa3-aa67-0ef4b529d04f");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18332c0-666b-4af8-bd17-00bd9344e575",
                column: "ConcurrencyStamp",
                value: "32a00378-0627-48d1-adf8-1b78d988ade9");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18332c0-admi-naf8-bd17-00bd9344e575",
                column: "ConcurrencyStamp",
                value: "d99dbfab-d2d6-42d4-96e8-ecd19b64d7f4");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18332c0-mode-rato-rd17-00bd9344e575",
                column: "ConcurrencyStamp",
                value: "552ab0ef-dea3-4b71-9c18-d4e6ca23c95c");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "b01bex90-admi-naf8-bd17-00bd9344e575",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "af3ea060-6841-4061-ad22-c380a917131f", "AQAAAAEAACcQAAAAECcvvG9oFmbbkzM67AciTGcyQua+bQGXQcKCQOxIFfVbqhwM8tXMDCmHjT3Yu57Baw==" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "b01bex90-stud-ent8-bd17-00bd9344e575",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "95fd4aa6-bcc5-4f7f-9b90-9df6cac49a3a", "AQAAAAEAACcQAAAAEEumGwn/qaQ8r2PnvDTU569xWK6W/2r7zRZPlvCymi0mFaXheXKYejwD/nbGFKcY1A==" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "b01rektor-admi-naf8-bd17-00bd9344e575",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "7f1dd850-417b-41f0-ad24-d2722b8d4ec5", "AQAAAAEAACcQAAAAEFkR2v9sDaonyM5x31t0i/ZG2/eJl5EaOmweWKlUp9GfYjDruIijG2VntgAKDoT1TA==" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "bdziek4n-admi-naf8-bd17-00bd9344e575",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "186d5951-b75b-4fa8-a95e-b24daf1a850b", "AQAAAAEAACcQAAAAEJsOCGTxMQFLir3pPquTKoMdKiO135gRREqmurvk3DSUna/zJyC1TQFxtaeCsyQ0ew==" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "bdziek4n-admi-naf8-bd17-pracownik4e575",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "0d7ca44f-5700-4f51-9470-313b726ee1d4", "AQAAAAEAACcQAAAAEOIoDLqlBuGcyuchFfHsGUPKjC268rIc0i96bckd/aS2aknEw6vpIMpmTkAg/AhOXw==" });

            migrationBuilder.AddForeignKey(
                name: "FK_DocumentActivities_Documents_DocumentId",
                table: "DocumentActivities",
                column: "DocumentId",
                principalTable: "Documents",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
