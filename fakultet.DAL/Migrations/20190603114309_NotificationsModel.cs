﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace fakultet.DAL.Migrations
{
    public partial class NotificationsModel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Notifications",
                columns: table => new
                {
                    NotificationId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DocumentId = table.Column<int>(nullable: false),
                    Content = table.Column<string>(nullable: true),
                    SecondaryContent = table.Column<string>(nullable: true),
                    Type = table.Column<int>(nullable: false),
                    Read = table.Column<bool>(nullable: false),
                    RecipientId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Notifications", x => x.NotificationId);
                    table.ForeignKey(
                        name: "FK_Notifications_AspNetUsers_RecipientId",
                        column: x => x.RecipientId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18332c0-420b-4af8-bd17-00bd9344e575",
                column: "ConcurrencyStamp",
                value: "a48074f1-3fbd-4ca1-a71b-0b4ec5c1ae79");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18332c0-666b-4af8-bd17-00bd9344e575",
                column: "ConcurrencyStamp",
                value: "195856ad-7273-4ec0-852a-3aa736835499");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18332c0-admi-naf8-bd17-00bd9344e575",
                column: "ConcurrencyStamp",
                value: "f6284a3e-2837-450d-baf1-d92cd9f200c5");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18332c0-mode-rato-rd17-00bd9344e575",
                column: "ConcurrencyStamp",
                value: "2380b194-ae63-4e1c-83f6-9587ef747709");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "b01bex90-admi-naf8-bd17-00bd9344e575",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "cdb2d481-f6f3-42d5-9fce-fcb0d8012bba", "AQAAAAEAACcQAAAAEN267x2kXnNIzOL4fGbKzKWynWzEyJKsmPkmOez2IMeP9/xdr+u/9k9DpFpBz2PpEg==" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "b01bex90-stud-ent8-bd17-00bd9344e575",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "5ef745ba-ec4e-42c2-97d9-79af4aba2e64", "AQAAAAEAACcQAAAAEMEvSSZKrMw/vp6CySld/8GR3hBc/Z+dGPCcZlrLdnbT9kbFv8Ml2PlzL7qYbdblXA==" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "b01rektor-admi-naf8-bd17-00bd9344e575",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "9a21ceec-a45e-41ca-ad8b-3b70b5b7c33e", "AQAAAAEAACcQAAAAEKKDLav9MpdJwyKIYSimTlENAXUWrAzxurXop9EcS7e4d0DgvkRs0nhFeBIYGcNyxw==" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "bdziek4n-admi-naf8-bd17-00bd9344e575",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "c109beac-d705-4b1a-89c0-344fae1702d9", "AQAAAAEAACcQAAAAEJj8QD9gh/4LWGr3tVEDEtGXeWxLhtDjD++B2Tk+BZxzvvmBg0DsQPDvg6OktwBlnQ==" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "bdziek4n-admi-naf8-bd17-pracownik4e575",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "3f5d3c62-ccfa-4a7e-896d-060c3b7f08b9", "AQAAAAEAACcQAAAAEDudFKj+rbcppPVmS1n+dZp4kykjNGiTQUg0yEMgG08FGLVuUdKkXoUogf47/iZwRg==" });

            migrationBuilder.CreateIndex(
                name: "IX_Notifications_RecipientId",
                table: "Notifications",
                column: "RecipientId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Notifications");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18332c0-420b-4af8-bd17-00bd9344e575",
                column: "ConcurrencyStamp",
                value: "20f847be-92a7-4b46-9f33-c1d8025d3e1f");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18332c0-666b-4af8-bd17-00bd9344e575",
                column: "ConcurrencyStamp",
                value: "99821a72-084a-4519-a81c-c8ee71c96229");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18332c0-admi-naf8-bd17-00bd9344e575",
                column: "ConcurrencyStamp",
                value: "62bef42e-ef72-4cfa-a609-50476d6e08da");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18332c0-mode-rato-rd17-00bd9344e575",
                column: "ConcurrencyStamp",
                value: "f3a9cf01-86d9-44d1-8c5b-89e7a45cbb05");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "b01bex90-admi-naf8-bd17-00bd9344e575",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "9785f8b5-48a1-4783-bcf4-655dc3ea64cc", "AQAAAAEAACcQAAAAEKEaf5r/lp75XQEgZohRT5C1ZOUvbTVfuJ4mc75D5u/zBc7vQEJDI1ZbBZe+0NQSfQ==" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "b01bex90-stud-ent8-bd17-00bd9344e575",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "71e28b9b-f739-4130-9a85-1851c8e7f15b", "AQAAAAEAACcQAAAAEA9iTrxNwOZi6spN2T3Tb5xDOUIWbIHfSHIaIJrCqx7F9zC4PA9ntXJgnkynoofT3w==" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "b01rektor-admi-naf8-bd17-00bd9344e575",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "6c06f8a4-508b-45e6-99f0-1de81b5d0d68", "AQAAAAEAACcQAAAAEPgtrU+S5CsIrfqPg7Wxz2FiW2jD2Tk9SdQm4OcBrpJfm8d6AfQtpXkxOcjIUSClrg==" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "bdziek4n-admi-naf8-bd17-00bd9344e575",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "924b9fef-919b-4611-b600-44aeb4d8507c", "AQAAAAEAACcQAAAAEMEJPxrbF1cj40ddUvulYiT2dlhH43/xZa4DY/N0ZTZcVLhtXRI5oWTsIUJmAbYDCQ==" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "bdziek4n-admi-naf8-bd17-pracownik4e575",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "b3867442-0ff2-4c23-ae21-b28ab4b7d0c6", "AQAAAAEAACcQAAAAEN8/dwff88tAC7hnuxb1Dg0xZsFHkW9L6eieGyugaUeQaSxGwxERtK/UO5JJbTIw3Q==" });
        }
    }
}
