﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace fakultet.DAL.Migrations
{
    public partial class DocumentTypeAllowedToSendColumn : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "AllowedToSendUserAccount",
                table: "DocumentTypes",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18332c0-420b-4af8-bd17-00bd9344e575",
                column: "ConcurrencyStamp",
                value: "ea50ca38-ed67-46d5-a915-9d560bd3a47c");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18332c0-666b-4af8-bd17-00bd9344e575",
                column: "ConcurrencyStamp",
                value: "70a44099-e525-4314-81dc-69563dcd5da1");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18332c0-admi-naf8-bd17-00bd9344e575",
                column: "ConcurrencyStamp",
                value: "7903d670-143c-47c7-923c-3290fdedb8eb");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18332c0-mode-rato-rd17-00bd9344e575",
                column: "ConcurrencyStamp",
                value: "652f9fae-4123-4c58-bac3-9b918ac82843");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "b01bex90-admi-naf8-bd17-00bd9344e575",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "098dd027-7e53-4c04-9a92-d9a5e3e6fc13", "AQAAAAEAACcQAAAAEJNCsVoF40G6CUkwLv/0C26HQhAuKxuvCBPtIjkFEZHNiBOcT7uxYEsK7Qy99KDB0A==" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "b01bex90-stud-ent8-bd17-00bd9344e575",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "c90f5566-d281-4c70-a67e-1ff96fdc55ba", "AQAAAAEAACcQAAAAEI1MpL4QlThjs66qGzwo+VDFOElgroOgy0Auaj0neyE5rZ7kYudFqPA3fIJotTgSmQ==" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "b01rektor-admi-naf8-bd17-00bd9344e575",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "0375ac52-d4a7-407c-a283-a65c4ee4aefe", "AQAAAAEAACcQAAAAEJUpdbSFNs5UDhk0g+oFSGkeN+uLFdeBI7puqbbRd1qTNKnS7tx3iwns9YzEhtFE2g==" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "bdziek4n-admi-naf8-bd17-00bd9344e575",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "2959b070-3406-4e0f-9e4c-70468e697d21", "AQAAAAEAACcQAAAAEFJXgdFma8IAVXzmUKDnTNlGV+ywoz5TJAqLKL9fe9KzxEMGx6S3gFlw1sIiveAsog==" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "bdziek4n-admi-naf8-bd17-pracownik4e575",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "2726a039-f6c7-43b9-8eb8-1e0a01b0b527", "AQAAAAEAACcQAAAAEEG5h1w0JoSoPLAllnaURITL9hyNlbWtO8VcBnBgnylafso58ekAuAf5KQ3Bclwdrg==" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AllowedToSendUserAccount",
                table: "DocumentTypes");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18332c0-420b-4af8-bd17-00bd9344e575",
                column: "ConcurrencyStamp",
                value: "5454a6f0-4482-41b9-ba29-b44c294f1014");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18332c0-666b-4af8-bd17-00bd9344e575",
                column: "ConcurrencyStamp",
                value: "95250610-f796-4d40-bc74-d52247d05e16");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18332c0-admi-naf8-bd17-00bd9344e575",
                column: "ConcurrencyStamp",
                value: "18d675ad-e775-4a02-b801-fb7a06f555bf");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18332c0-mode-rato-rd17-00bd9344e575",
                column: "ConcurrencyStamp",
                value: "a7272de5-c184-431f-95ef-b1b61de50eb0");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "b01bex90-admi-naf8-bd17-00bd9344e575",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "f6926631-2218-4728-9518-44938195f9c1", "AQAAAAEAACcQAAAAENu0rEn8mnma3ogEYbE/N3DGxkFHdGuznHHwW5oLN3VUd3JnviytAVVMy7zlBbwm4g==" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "b01bex90-stud-ent8-bd17-00bd9344e575",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "3352f423-64b7-492c-8dc5-d3a685df8f9f", "AQAAAAEAACcQAAAAEGp3zm6oa1WN+5vl60CPHAY7aqWFVjH70hvwshkSBIhE8R8OjbxtK39M/l9ZGhcZJQ==" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "b01rektor-admi-naf8-bd17-00bd9344e575",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "0fbb1a8c-cdff-4d22-be0b-8491a650e8ff", "AQAAAAEAACcQAAAAEKACSwKmrSXiaI23eTYodArQs7OQZWNYOptXAcVa0O5VLy9SnJucTJzzERvypW69EA==" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "bdziek4n-admi-naf8-bd17-00bd9344e575",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "a44eb54d-f784-4884-af46-65c200721220", "AQAAAAEAACcQAAAAEH4FtnIumShtaoY8r7QKfeShy6wXDxoY8yD9DIWJZRmiHjNuao4D7FngD3AiH3uBbQ==" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "bdziek4n-admi-naf8-bd17-pracownik4e575",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "38a59682-bcac-4511-b94d-345086317f8a", "AQAAAAEAACcQAAAAEHKjIuMtaTh8qntI0H1RDYKQMjV5lD26PBa1jVRI9vEjnJoxJWpitOJd/apdFgHfUA==" });
        }
    }
}
