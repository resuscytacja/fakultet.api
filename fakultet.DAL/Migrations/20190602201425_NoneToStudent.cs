﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace fakultet.DAL.Migrations
{
    public partial class NoneToStudent : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18332c0-420b-4af8-bd17-00bd9344e575",
                column: "ConcurrencyStamp",
                value: "20f847be-92a7-4b46-9f33-c1d8025d3e1f");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18332c0-666b-4af8-bd17-00bd9344e575",
                column: "ConcurrencyStamp",
                value: "99821a72-084a-4519-a81c-c8ee71c96229");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18332c0-admi-naf8-bd17-00bd9344e575",
                column: "ConcurrencyStamp",
                value: "62bef42e-ef72-4cfa-a609-50476d6e08da");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18332c0-mode-rato-rd17-00bd9344e575",
                column: "ConcurrencyStamp",
                value: "f3a9cf01-86d9-44d1-8c5b-89e7a45cbb05");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "b01bex90-admi-naf8-bd17-00bd9344e575",
                columns: new[] { "ConcurrencyStamp", "JobPosition", "PasswordHash" },
                values: new object[] { "9785f8b5-48a1-4783-bcf4-655dc3ea64cc", 4, "AQAAAAEAACcQAAAAEKEaf5r/lp75XQEgZohRT5C1ZOUvbTVfuJ4mc75D5u/zBc7vQEJDI1ZbBZe+0NQSfQ==" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "b01bex90-stud-ent8-bd17-00bd9344e575",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "71e28b9b-f739-4130-9a85-1851c8e7f15b", "AQAAAAEAACcQAAAAEA9iTrxNwOZi6spN2T3Tb5xDOUIWbIHfSHIaIJrCqx7F9zC4PA9ntXJgnkynoofT3w==" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "b01rektor-admi-naf8-bd17-00bd9344e575",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "6c06f8a4-508b-45e6-99f0-1de81b5d0d68", "AQAAAAEAACcQAAAAEPgtrU+S5CsIrfqPg7Wxz2FiW2jD2Tk9SdQm4OcBrpJfm8d6AfQtpXkxOcjIUSClrg==" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "bdziek4n-admi-naf8-bd17-00bd9344e575",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "924b9fef-919b-4611-b600-44aeb4d8507c", "AQAAAAEAACcQAAAAEMEJPxrbF1cj40ddUvulYiT2dlhH43/xZa4DY/N0ZTZcVLhtXRI5oWTsIUJmAbYDCQ==" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "bdziek4n-admi-naf8-bd17-pracownik4e575",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "b3867442-0ff2-4c23-ae21-b28ab4b7d0c6", "AQAAAAEAACcQAAAAEN8/dwff88tAC7hnuxb1Dg0xZsFHkW9L6eieGyugaUeQaSxGwxERtK/UO5JJbTIw3Q==" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18332c0-420b-4af8-bd17-00bd9344e575",
                column: "ConcurrencyStamp",
                value: "ea50ca38-ed67-46d5-a915-9d560bd3a47c");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18332c0-666b-4af8-bd17-00bd9344e575",
                column: "ConcurrencyStamp",
                value: "70a44099-e525-4314-81dc-69563dcd5da1");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18332c0-admi-naf8-bd17-00bd9344e575",
                column: "ConcurrencyStamp",
                value: "7903d670-143c-47c7-923c-3290fdedb8eb");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18332c0-mode-rato-rd17-00bd9344e575",
                column: "ConcurrencyStamp",
                value: "652f9fae-4123-4c58-bac3-9b918ac82843");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "b01bex90-admi-naf8-bd17-00bd9344e575",
                columns: new[] { "ConcurrencyStamp", "JobPosition", "PasswordHash" },
                values: new object[] { "098dd027-7e53-4c04-9a92-d9a5e3e6fc13", 0, "AQAAAAEAACcQAAAAEJNCsVoF40G6CUkwLv/0C26HQhAuKxuvCBPtIjkFEZHNiBOcT7uxYEsK7Qy99KDB0A==" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "b01bex90-stud-ent8-bd17-00bd9344e575",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "c90f5566-d281-4c70-a67e-1ff96fdc55ba", "AQAAAAEAACcQAAAAEI1MpL4QlThjs66qGzwo+VDFOElgroOgy0Auaj0neyE5rZ7kYudFqPA3fIJotTgSmQ==" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "b01rektor-admi-naf8-bd17-00bd9344e575",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "0375ac52-d4a7-407c-a283-a65c4ee4aefe", "AQAAAAEAACcQAAAAEJUpdbSFNs5UDhk0g+oFSGkeN+uLFdeBI7puqbbRd1qTNKnS7tx3iwns9YzEhtFE2g==" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "bdziek4n-admi-naf8-bd17-00bd9344e575",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "2959b070-3406-4e0f-9e4c-70468e697d21", "AQAAAAEAACcQAAAAEFJXgdFma8IAVXzmUKDnTNlGV+ywoz5TJAqLKL9fe9KzxEMGx6S3gFlw1sIiveAsog==" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "bdziek4n-admi-naf8-bd17-pracownik4e575",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "2726a039-f6c7-43b9-8eb8-1e0a01b0b527", "AQAAAAEAACcQAAAAEEG5h1w0JoSoPLAllnaURITL9hyNlbWtO8VcBnBgnylafso58ekAuAf5KQ3Bclwdrg==" });
        }
    }
}
