﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace fakultet.DAL.Migrations
{
    public partial class UpdateModelDocuments : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Documents_AspNetUsers_ConcelledById",
                table: "Documents");

            migrationBuilder.DropForeignKey(
                name: "FK_Documents_AspNetUsers_UnverifiedById",
                table: "Documents");

            migrationBuilder.DropIndex(
                name: "IX_Documents_ConcelledById",
                table: "Documents");

            migrationBuilder.RenameColumn(
                name: "UnverifiedById",
                table: "Documents",
                newName: "VerificationById");

            migrationBuilder.RenameColumn(
                name: "ResolveTime",
                table: "Documents",
                newName: "VerificationTime");

            migrationBuilder.RenameColumn(
                name: "ConcelledById",
                table: "Documents",
                newName: "ReturnedReason");

            migrationBuilder.RenameIndex(
                name: "IX_Documents_UnverifiedById",
                table: "Documents",
                newName: "IX_Documents_VerificationById");

            migrationBuilder.AlterColumn<string>(
                name: "ReturnedReason",
                table: "Documents",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ConsiderationById",
                table: "Documents",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ConsiderationTime",
                table: "Documents",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18332c0-420b-4af8-bd17-00bd9344e575",
                column: "ConcurrencyStamp",
                value: "5454a6f0-4482-41b9-ba29-b44c294f1014");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18332c0-666b-4af8-bd17-00bd9344e575",
                column: "ConcurrencyStamp",
                value: "95250610-f796-4d40-bc74-d52247d05e16");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18332c0-admi-naf8-bd17-00bd9344e575",
                column: "ConcurrencyStamp",
                value: "18d675ad-e775-4a02-b801-fb7a06f555bf");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18332c0-mode-rato-rd17-00bd9344e575",
                column: "ConcurrencyStamp",
                value: "a7272de5-c184-431f-95ef-b1b61de50eb0");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "b01bex90-admi-naf8-bd17-00bd9344e575",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "f6926631-2218-4728-9518-44938195f9c1", "AQAAAAEAACcQAAAAENu0rEn8mnma3ogEYbE/N3DGxkFHdGuznHHwW5oLN3VUd3JnviytAVVMy7zlBbwm4g==" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "b01bex90-stud-ent8-bd17-00bd9344e575",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "3352f423-64b7-492c-8dc5-d3a685df8f9f", "AQAAAAEAACcQAAAAEGp3zm6oa1WN+5vl60CPHAY7aqWFVjH70hvwshkSBIhE8R8OjbxtK39M/l9ZGhcZJQ==" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "b01rektor-admi-naf8-bd17-00bd9344e575",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "0fbb1a8c-cdff-4d22-be0b-8491a650e8ff", "AQAAAAEAACcQAAAAEKACSwKmrSXiaI23eTYodArQs7OQZWNYOptXAcVa0O5VLy9SnJucTJzzERvypW69EA==" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "bdziek4n-admi-naf8-bd17-00bd9344e575",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "a44eb54d-f784-4884-af46-65c200721220", "AQAAAAEAACcQAAAAEH4FtnIumShtaoY8r7QKfeShy6wXDxoY8yD9DIWJZRmiHjNuao4D7FngD3AiH3uBbQ==" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "bdziek4n-admi-naf8-bd17-pracownik4e575",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "38a59682-bcac-4511-b94d-345086317f8a", "AQAAAAEAACcQAAAAEHKjIuMtaTh8qntI0H1RDYKQMjV5lD26PBa1jVRI9vEjnJoxJWpitOJd/apdFgHfUA==" });

            migrationBuilder.CreateIndex(
                name: "IX_Documents_ConsiderationById",
                table: "Documents",
                column: "ConsiderationById");

            migrationBuilder.AddForeignKey(
                name: "FK_Documents_AspNetUsers_ConsiderationById",
                table: "Documents",
                column: "ConsiderationById",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Documents_AspNetUsers_VerificationById",
                table: "Documents",
                column: "VerificationById",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Documents_AspNetUsers_ConsiderationById",
                table: "Documents");

            migrationBuilder.DropForeignKey(
                name: "FK_Documents_AspNetUsers_VerificationById",
                table: "Documents");

            migrationBuilder.DropIndex(
                name: "IX_Documents_ConsiderationById",
                table: "Documents");

            migrationBuilder.DropColumn(
                name: "ConsiderationById",
                table: "Documents");

            migrationBuilder.DropColumn(
                name: "ConsiderationTime",
                table: "Documents");

            migrationBuilder.RenameColumn(
                name: "VerificationTime",
                table: "Documents",
                newName: "ResolveTime");

            migrationBuilder.RenameColumn(
                name: "VerificationById",
                table: "Documents",
                newName: "UnverifiedById");

            migrationBuilder.RenameColumn(
                name: "ReturnedReason",
                table: "Documents",
                newName: "ConcelledById");

            migrationBuilder.RenameIndex(
                name: "IX_Documents_VerificationById",
                table: "Documents",
                newName: "IX_Documents_UnverifiedById");

            migrationBuilder.AlterColumn<string>(
                name: "ConcelledById",
                table: "Documents",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18332c0-420b-4af8-bd17-00bd9344e575",
                column: "ConcurrencyStamp",
                value: "5193720c-24c3-4941-8b3b-1a0b72246cf3");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18332c0-666b-4af8-bd17-00bd9344e575",
                column: "ConcurrencyStamp",
                value: "19c9ecf6-0b8e-42d0-9ec0-d4751171c523");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18332c0-admi-naf8-bd17-00bd9344e575",
                column: "ConcurrencyStamp",
                value: "efce5206-8dcb-479a-92ac-163da3058c8c");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a18332c0-mode-rato-rd17-00bd9344e575",
                column: "ConcurrencyStamp",
                value: "eae59da8-8f7b-437d-a662-9a0b43ac16ea");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "b01bex90-admi-naf8-bd17-00bd9344e575",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "5f5a0ea0-c5f9-4650-a553-79566c2a840a", "AQAAAAEAACcQAAAAEEo6EzHYcPr3Ul+fClFKskpIjeF/Oa+wCefwVuC3uCIZ/eGEuIOdtw5rx7TIePQ/NQ==" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "b01bex90-stud-ent8-bd17-00bd9344e575",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "190cf241-5250-4c6d-8cde-fe7bc13b0cef", "AQAAAAEAACcQAAAAEIqcFbc9JzhE3rABoyn2AfXzqvkhBSS242Ebt7idrTioqBw+nFom+OftSqa/pGZiMQ==" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "b01rektor-admi-naf8-bd17-00bd9344e575",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "e7ed8806-c61a-48ba-8e20-31055fb0faeb", "AQAAAAEAACcQAAAAEDjzJA0VHJBnZmQyk5GRH4imRwD0XpXlSg32n5u6d7ogE+shmQebq7sFxUft3qzqAQ==" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "bdziek4n-admi-naf8-bd17-00bd9344e575",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "808c84f9-fa41-4b39-8ead-caed514798df", "AQAAAAEAACcQAAAAEM3Xs7xSW4RUwqr3ITidlPrxUurFh15BJ7ga93eBybEfh7bS7BdiZMdnpKyTSM7Gsg==" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "bdziek4n-admi-naf8-bd17-pracownik4e575",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "73535c35-4b7d-48df-b3bb-6d8fdf1f1476", "AQAAAAEAACcQAAAAEI+UjMZeZYh+f6pee8o37GbLSaWcRbthrSiq5PHY3dwVv2RmcCMOZFKDfQvRBN2ZRQ==" });

            migrationBuilder.CreateIndex(
                name: "IX_Documents_ConcelledById",
                table: "Documents",
                column: "ConcelledById");

            migrationBuilder.AddForeignKey(
                name: "FK_Documents_AspNetUsers_ConcelledById",
                table: "Documents",
                column: "ConcelledById",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Documents_AspNetUsers_UnverifiedById",
                table: "Documents",
                column: "UnverifiedById",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
