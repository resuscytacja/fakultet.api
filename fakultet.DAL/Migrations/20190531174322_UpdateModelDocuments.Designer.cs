﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using fakultet.DAL;

namespace fakultet.DAL.Migrations
{
    [DbContext(typeof(AppDbContext))]
    [Migration("20190531174322_UpdateModelDocuments")]
    partial class UpdateModelDocuments
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.2.4-servicing-10062")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityRole", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("Name")
                        .HasMaxLength(256);

                    b.Property<string>("NormalizedName")
                        .HasMaxLength(256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedName")
                        .IsUnique()
                        .HasName("RoleNameIndex")
                        .HasFilter("[NormalizedName] IS NOT NULL");

                    b.ToTable("AspNetRoles");

                    b.HasData(
                        new
                        {
                            Id = "a18332c0-666b-4af8-bd17-00bd9344e575",
                            ConcurrencyStamp = "95250610-f796-4d40-bc74-d52247d05e16",
                            Name = "Student",
                            NormalizedName = "STUDENT"
                        },
                        new
                        {
                            Id = "a18332c0-420b-4af8-bd17-00bd9344e575",
                            ConcurrencyStamp = "5454a6f0-4482-41b9-ba29-b44c294f1014",
                            Name = "Superadmin",
                            NormalizedName = "SUPERADMIN"
                        },
                        new
                        {
                            Id = "a18332c0-admi-naf8-bd17-00bd9344e575",
                            ConcurrencyStamp = "18d675ad-e775-4a02-b801-fb7a06f555bf",
                            Name = "Admin",
                            NormalizedName = "ADMIN"
                        },
                        new
                        {
                            Id = "a18332c0-mode-rato-rd17-00bd9344e575",
                            ConcurrencyStamp = "a7272de5-c184-431f-95ef-b1b61de50eb0",
                            Name = "Moderator",
                            NormalizedName = "MODERATOR"
                        });
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityRoleClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<string>("RoleId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetRoleClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<string>("UserId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserLogin<string>", b =>
                {
                    b.Property<string>("LoginProvider");

                    b.Property<string>("ProviderKey");

                    b.Property<string>("ProviderDisplayName");

                    b.Property<string>("UserId")
                        .IsRequired();

                    b.HasKey("LoginProvider", "ProviderKey");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserLogins");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserRole<string>", b =>
                {
                    b.Property<string>("UserId");

                    b.Property<string>("RoleId");

                    b.HasKey("UserId", "RoleId");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetUserRoles");

                    b.HasData(
                        new
                        {
                            UserId = "b01bex90-stud-ent8-bd17-00bd9344e575",
                            RoleId = "a18332c0-666b-4af8-bd17-00bd9344e575"
                        },
                        new
                        {
                            UserId = "b01bex90-admi-naf8-bd17-00bd9344e575",
                            RoleId = "a18332c0-420b-4af8-bd17-00bd9344e575"
                        },
                        new
                        {
                            UserId = "b01rektor-admi-naf8-bd17-00bd9344e575",
                            RoleId = "a18332c0-admi-naf8-bd17-00bd9344e575"
                        },
                        new
                        {
                            UserId = "bdziek4n-admi-naf8-bd17-00bd9344e575",
                            RoleId = "a18332c0-admi-naf8-bd17-00bd9344e575"
                        },
                        new
                        {
                            UserId = "bdziek4n-admi-naf8-bd17-pracownik4e575",
                            RoleId = "a18332c0-mode-rato-rd17-00bd9344e575"
                        });
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserToken<string>", b =>
                {
                    b.Property<string>("UserId");

                    b.Property<string>("LoginProvider");

                    b.Property<string>("Name");

                    b.Property<string>("Value");

                    b.HasKey("UserId", "LoginProvider", "Name");

                    b.ToTable("AspNetUserTokens");
                });

            modelBuilder.Entity("fakultet.DAL.Models.Document", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("CancelledReason");

                    b.Property<string>("ConsiderationById");

                    b.Property<DateTime?>("ConsiderationTime");

                    b.Property<string>("CreatedById");

                    b.Property<int?>("DocumentTypeId");

                    b.Property<string>("ReturnedReason");

                    b.Property<DateTime>("SendTime");

                    b.Property<int>("Status");

                    b.Property<string>("Title");

                    b.Property<string>("UnverifiedReason");

                    b.Property<string>("VerificationById");

                    b.Property<DateTime?>("VerificationTime");

                    b.HasKey("Id");

                    b.HasIndex("ConsiderationById");

                    b.HasIndex("CreatedById");

                    b.HasIndex("DocumentTypeId");

                    b.HasIndex("VerificationById");

                    b.ToTable("Documents");
                });

            modelBuilder.Entity("fakultet.DAL.Models.DocumentActivity", b =>
                {
                    b.Property<int>("DocumentActivityId")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime>("DateTime");

                    b.Property<int>("DocumentId");

                    b.Property<string>("InitiatedById");

                    b.Property<string>("Message");

                    b.Property<int>("Type");

                    b.HasKey("DocumentActivityId");

                    b.HasIndex("DocumentId");

                    b.HasIndex("InitiatedById");

                    b.ToTable("DocumentActivities");
                });

            modelBuilder.Entity("fakultet.DAL.Models.DocumentType", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("AcceptingUserAccount");

                    b.Property<string>("Name");

                    b.Property<int>("VerifyingUserAccount");

                    b.HasKey("Id");

                    b.ToTable("DocumentTypes");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            AcceptingUserAccount = 2,
                            Name = "Stypendia",
                            VerifyingUserAccount = 1
                        },
                        new
                        {
                            Id = 2,
                            AcceptingUserAccount = 2,
                            Name = "Warunki",
                            VerifyingUserAccount = 1
                        },
                        new
                        {
                            Id = 3,
                            AcceptingUserAccount = 2,
                            Name = "Urlop dziekański",
                            VerifyingUserAccount = 1
                        },
                        new
                        {
                            Id = 4,
                            AcceptingUserAccount = 2,
                            Name = "Podanie o dyplom",
                            VerifyingUserAccount = 1
                        },
                        new
                        {
                            Id = 5,
                            AcceptingUserAccount = 2,
                            Name = "Wypis z uczelni",
                            VerifyingUserAccount = 1
                        },
                        new
                        {
                            Id = 6,
                            AcceptingUserAccount = 3,
                            Name = "Zmiana kierunku studiów",
                            VerifyingUserAccount = 1
                        },
                        new
                        {
                            Id = 7,
                            AcceptingUserAccount = 3,
                            Name = "Zmiana terminu pracy dyplomowej",
                            VerifyingUserAccount = 1
                        },
                        new
                        {
                            Id = 8,
                            AcceptingUserAccount = 3,
                            Name = "Ponowne powtarzanie semestru",
                            VerifyingUserAccount = 1
                        });
                });

            modelBuilder.Entity("fakultet.DAL.Models.Files", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int?>("DocumentId");

                    b.Property<string>("FileName");

                    b.HasKey("Id");

                    b.HasIndex("DocumentId");

                    b.ToTable("Files");
                });

            modelBuilder.Entity("fakultet.DAL.Models.UserAccount", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("AccessFailedCount");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("Email")
                        .HasMaxLength(256);

                    b.Property<bool>("EmailConfirmed");

                    b.Property<string>("FirstName");

                    b.Property<int>("JobPosition");

                    b.Property<string>("LastName");

                    b.Property<bool>("LockoutEnabled");

                    b.Property<DateTimeOffset?>("LockoutEnd");

                    b.Property<string>("NormalizedEmail")
                        .HasMaxLength(256);

                    b.Property<string>("NormalizedUserName")
                        .HasMaxLength(256);

                    b.Property<string>("NumerIndeksu");

                    b.Property<string>("PasswordHash");

                    b.Property<string>("PhoneNumber");

                    b.Property<bool>("PhoneNumberConfirmed");

                    b.Property<string>("SecurityStamp");

                    b.Property<bool>("TwoFactorEnabled");

                    b.Property<string>("UserName")
                        .HasMaxLength(256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedEmail")
                        .HasName("EmailIndex");

                    b.HasIndex("NormalizedUserName")
                        .IsUnique()
                        .HasName("UserNameIndex")
                        .HasFilter("[NormalizedUserName] IS NOT NULL");

                    b.ToTable("AspNetUsers");

                    b.HasData(
                        new
                        {
                            Id = "b01bex90-admi-naf8-bd17-00bd9344e575",
                            AccessFailedCount = 0,
                            ConcurrencyStamp = "f6926631-2218-4728-9518-44938195f9c1",
                            Email = "admin@fakultet.pl",
                            EmailConfirmed = true,
                            FirstName = "Pan",
                            JobPosition = 0,
                            LastName = "SuperAdmin",
                            LockoutEnabled = false,
                            NormalizedEmail = "ADMIN@FAKULTET.pl",
                            NormalizedUserName = "SUPERADMIN123",
                            PasswordHash = "AQAAAAEAACcQAAAAENu0rEn8mnma3ogEYbE/N3DGxkFHdGuznHHwW5oLN3VUd3JnviytAVVMy7zlBbwm4g==",
                            PhoneNumberConfirmed = false,
                            SecurityStamp = "",
                            TwoFactorEnabled = false,
                            UserName = "superadmin123"
                        },
                        new
                        {
                            Id = "b01rektor-admi-naf8-bd17-00bd9344e575",
                            AccessFailedCount = 0,
                            ConcurrencyStamp = "0fbb1a8c-cdff-4d22-be0b-8491a650e8ff",
                            Email = "rektor@fakultet.pl",
                            EmailConfirmed = true,
                            FirstName = "Michał",
                            JobPosition = 3,
                            LastName = "Szczytny",
                            LockoutEnabled = false,
                            NormalizedEmail = "REKTOR@FAKULTET.PL",
                            NormalizedUserName = "REKTOR@FAKULTET.PL",
                            PasswordHash = "AQAAAAEAACcQAAAAEKACSwKmrSXiaI23eTYodArQs7OQZWNYOptXAcVa0O5VLy9SnJucTJzzERvypW69EA==",
                            PhoneNumberConfirmed = false,
                            SecurityStamp = "",
                            TwoFactorEnabled = false,
                            UserName = "rektor@fakultet.pl"
                        },
                        new
                        {
                            Id = "bdziek4n-admi-naf8-bd17-00bd9344e575",
                            AccessFailedCount = 0,
                            ConcurrencyStamp = "a44eb54d-f784-4884-af46-65c200721220",
                            Email = "dziekan@fakultet.pl",
                            EmailConfirmed = true,
                            FirstName = "Jakub",
                            JobPosition = 2,
                            LastName = "Marzyciel",
                            LockoutEnabled = false,
                            NormalizedEmail = "DZIEKAN@FAKULTET.PL",
                            NormalizedUserName = "DZIEKAN@FAKULTET.PL",
                            PasswordHash = "AQAAAAEAACcQAAAAEH4FtnIumShtaoY8r7QKfeShy6wXDxoY8yD9DIWJZRmiHjNuao4D7FngD3AiH3uBbQ==",
                            PhoneNumberConfirmed = false,
                            SecurityStamp = "",
                            TwoFactorEnabled = false,
                            UserName = "dziekan@fakultet.pl"
                        },
                        new
                        {
                            Id = "bdziek4n-admi-naf8-bd17-pracownik4e575",
                            AccessFailedCount = 0,
                            ConcurrencyStamp = "38a59682-bcac-4511-b94d-345086317f8a",
                            Email = "pracownikdziekanatu@fakultet.pl",
                            EmailConfirmed = true,
                            FirstName = "Patryk",
                            JobPosition = 1,
                            LastName = "Kyrtap",
                            LockoutEnabled = false,
                            NormalizedEmail = "PRACOWNIKDZIEKANATU@FAKULTET.PL",
                            NormalizedUserName = "PRACOWNIKDZIEKANATU@FAKULTET.PL",
                            PasswordHash = "AQAAAAEAACcQAAAAEHKjIuMtaTh8qntI0H1RDYKQMjV5lD26PBa1jVRI9vEjnJoxJWpitOJd/apdFgHfUA==",
                            PhoneNumberConfirmed = false,
                            SecurityStamp = "",
                            TwoFactorEnabled = false,
                            UserName = "pracownikdziekanatu@fakultet.pl"
                        },
                        new
                        {
                            Id = "b01bex90-stud-ent8-bd17-00bd9344e575",
                            AccessFailedCount = 0,
                            ConcurrencyStamp = "3352f423-64b7-492c-8dc5-d3a685df8f9f",
                            Email = "student@fakultet.pl",
                            EmailConfirmed = true,
                            FirstName = "Pilny",
                            JobPosition = 0,
                            LastName = "Student",
                            LockoutEnabled = false,
                            NormalizedEmail = "STUDENT@FAKULTET.pl",
                            NormalizedUserName = "STUDENT@FAKULTET.pl",
                            NumerIndeksu = "123456",
                            PasswordHash = "AQAAAAEAACcQAAAAEGp3zm6oa1WN+5vl60CPHAY7aqWFVjH70hvwshkSBIhE8R8OjbxtK39M/l9ZGhcZJQ==",
                            PhoneNumberConfirmed = false,
                            SecurityStamp = "",
                            TwoFactorEnabled = false,
                            UserName = "student@fakultet.pl"
                        });
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityRoleClaim<string>", b =>
                {
                    b.HasOne("Microsoft.AspNetCore.Identity.IdentityRole")
                        .WithMany()
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserClaim<string>", b =>
                {
                    b.HasOne("fakultet.DAL.Models.UserAccount")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserLogin<string>", b =>
                {
                    b.HasOne("fakultet.DAL.Models.UserAccount")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserRole<string>", b =>
                {
                    b.HasOne("Microsoft.AspNetCore.Identity.IdentityRole")
                        .WithMany()
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("fakultet.DAL.Models.UserAccount")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserToken<string>", b =>
                {
                    b.HasOne("fakultet.DAL.Models.UserAccount")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("fakultet.DAL.Models.Document", b =>
                {
                    b.HasOne("fakultet.DAL.Models.UserAccount", "ConsiderationBy")
                        .WithMany()
                        .HasForeignKey("ConsiderationById");

                    b.HasOne("fakultet.DAL.Models.UserAccount", "CreatedBy")
                        .WithMany()
                        .HasForeignKey("CreatedById");

                    b.HasOne("fakultet.DAL.Models.DocumentType", "DocumentType")
                        .WithMany()
                        .HasForeignKey("DocumentTypeId");

                    b.HasOne("fakultet.DAL.Models.UserAccount", "VerificationBy")
                        .WithMany()
                        .HasForeignKey("VerificationById");
                });

            modelBuilder.Entity("fakultet.DAL.Models.DocumentActivity", b =>
                {
                    b.HasOne("fakultet.DAL.Models.Document", "Document")
                        .WithMany("Activities")
                        .HasForeignKey("DocumentId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("fakultet.DAL.Models.UserAccount", "InitiatedBy")
                        .WithMany()
                        .HasForeignKey("InitiatedById");
                });

            modelBuilder.Entity("fakultet.DAL.Models.Files", b =>
                {
                    b.HasOne("fakultet.DAL.Models.Document", "Document")
                        .WithMany("FileList")
                        .HasForeignKey("DocumentId");
                });
#pragma warning restore 612, 618
        }
    }
}
