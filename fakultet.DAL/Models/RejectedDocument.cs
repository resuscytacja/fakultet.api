﻿using System;
using System.Collections.Generic;
using System.Text;

namespace fakultet.DAL.Models
{
    public class RejectedDocument
    {
        public int RejectedDocumentId { get; set; }
        public string Comment { get; set; }
        public UserAccount UserAccount { get; set; }

    }
}
