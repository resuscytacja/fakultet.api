﻿using fakultet.common;

namespace fakultet.DAL.Models
{
    public class Notification
    {
        public int NotificationId { get; set; }

        public int DocumentId { get; set; }

        public string Content { get; set; }

        public string SecondaryContent { get; set; }

        public NotificationTypeEnum Type { get; set; }

        public bool Read { get; set; }

        public UserAccount Recipient { get; set; }
    }
}
