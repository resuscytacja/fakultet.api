﻿using fakultet.common;
using Microsoft.AspNetCore.Identity;

namespace fakultet.DAL.Models
{
    public class UserAccount : IdentityUser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string NumerIndeksu { get; set; }
        public JobPositionEnum JobPosition { get; set; }
    }
}
