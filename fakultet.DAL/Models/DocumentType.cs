﻿using fakultet.common;

namespace fakultet.DAL.Models
{
    public class DocumentType
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public JobPositionEnum AllowedToSendUserAccount { get; set; } // TODO: używać listy upoważnionych osób (więcej niż 1 jobposition)
        public JobPositionEnum AcceptingUserAccount { get; set; }
        public JobPositionEnum VerifyingUserAccount { get; set; }
    }
}
