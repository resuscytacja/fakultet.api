﻿using System;
using System.Collections.Generic;
using System.Text;

namespace fakultet.DAL.Models
{
    public class Files
    {
        public int Id { get; set; }
        public string FileName { get; set; }
        public Document Document { get; set; }
    }
}
