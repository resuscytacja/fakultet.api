﻿using System;
using fakultet.common;

namespace fakultet.DAL.Models
{
    public class DocumentActivity
    {
        public int DocumentActivityId { get; set; }

        public DocumentActivityTypeEnum Type { get; set; }

        public Document Document { get; set; }
        public int DocumentId{ get; set; }

        public string Message { get; set; }

        public UserAccount InitiatedBy { get; set; }

        public DateTime DateTime { get; set; }
    }
}
