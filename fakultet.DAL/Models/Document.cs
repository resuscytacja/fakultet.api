﻿using System;
using System.Collections.Generic;
using fakultet.common;

namespace fakultet.DAL.Models
{
    public class Document
    {
        public int Id { get; set; }
        public DocumentType DocumentType { get; set; }
        public DocumentStatusEnum Status { get; set; }
        public string Title { get; set; }
        public DateTime SendTime { get; set; }
        public IList<Files> FileList { get; set; }
        public string UnverifiedReason { get; set; }
        public UserAccount VerificationBy { get; set; } //osoba któa nadała status Verified lub UnVerified
        public DateTime? VerificationTime { get; set; } // czas kiedy pani z dziekanantu zmieniła status z awaiting na Verified lub UnVerified
        public string CancelledReason { get; set; }
        public UserAccount ConsiderationBy { get; set; } //osoba któa nadała status Accepted lub Cancelled
        public DateTime? ConsiderationTime { get; set; } // czas kiedy rektor/dziekan zmienil status z Verified na Accepted lub Cancelled
        public string ReturnedReason { get; set; }
        public UserAccount CreatedBy { get; set; }
        public IList<DocumentActivity> Activities { get; set; }
    }
}
