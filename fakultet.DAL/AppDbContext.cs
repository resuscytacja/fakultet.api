﻿using fakultet.common;
using fakultet.DAL.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace fakultet.DAL
{
    public class AppDbContext : IdentityDbContext<UserAccount>
    {
        public AppDbContext(DbContextOptions<AppDbContext> options)
            : base(options)
        {
        }

        public DbSet<UserAccount> UserAccounts { get; set; }
        public DbSet<DocumentType> DocumentTypes { get; set; }
        public DbSet<Document> Documents { get; set; }
        public DbSet<Files> Files { get; set; }
        public DbSet<DocumentActivity> DocumentActivities { get; set; }
        public DbSet<Notification> Notifications { get; set; }


        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<DocumentActivity>()
                .HasOne<Document>(s => s.Document)
                .WithMany(g => g.Activities)
                .HasForeignKey(s => s.DocumentId);

            // zakomentowane, żeby nie seedowało przy każdym tworzeniu migracji
            #region Seedowanie bazy

            const string SUPERADMIN_ROLE_ID = "a18332c0-420b-4af8-bd17-00bd9344e575";
            const string ADMIN_ROLE_ID = "a18332c0-admi-naf8-bd17-00bd9344e575";
            const string MODERATOR_ROLE_ID = "a18332c0-mode-rato-rd17-00bd9344e575";
            const string STUDENT_ROLE_ID = "a18332c0-666b-4af8-bd17-00bd9344e575";

            const string STUDENT_ACCOUNT_ID = "b01bex90-stud-ent8-bd17-00bd9344e575";
            const string SUPERADMIN_ACCOUNT_ID = "b01bex90-admi-naf8-bd17-00bd9344e575";
            const string REKTOR_ACCOUNT_ID = "b01rektor-admi-naf8-bd17-00bd9344e575";
            const string DZIEKAN_ACCOUNT_ID = "bdziek4n-admi-naf8-bd17-00bd9344e575";
            const string PRACOWNIKDZIEKANATU_ACCOUNT_ID = "bdziek4n-admi-naf8-bd17-pracownik4e575";

            // seed roli użytkowników (Superadmin, Admin, Moderator, Student)
            builder.Entity<IdentityRole>().HasData(new IdentityRole
            {
                Id = STUDENT_ROLE_ID,
                Name = "Student",
                NormalizedName = "STUDENT"
            });
            builder.Entity<IdentityRole>().HasData(new IdentityRole
            {
                Id = SUPERADMIN_ROLE_ID,
                Name = "Superadmin",
                NormalizedName = "SUPERADMIN"
            });
            builder.Entity<IdentityRole>().HasData(new IdentityRole
            {
                Id = ADMIN_ROLE_ID,
                Name = "Admin",
                NormalizedName = "ADMIN"
            });
            builder.Entity<IdentityRole>().HasData(new IdentityRole
            {
                Id = MODERATOR_ROLE_ID,
                Name = "Moderator",
                NormalizedName = "MODERATOR"
            });

            var hasher = new PasswordHasher<UserAccount>();

            // seed kont użytkowników
            builder.Entity<UserAccount>().HasData(new UserAccount
            {
                Id = SUPERADMIN_ACCOUNT_ID,
                UserName = "superadmin123",
                NormalizedUserName = "SUPERADMIN123",
                Email = "admin@fakultet.pl",
                NormalizedEmail = "ADMIN@FAKULTET.pl",
                EmailConfirmed = true,
                PasswordHash = hasher.HashPassword(null, "superadmin123"),
                SecurityStamp = string.Empty,
                FirstName = "Pan",
                LastName = "SuperAdmin",
                NumerIndeksu = null,
                JobPosition = JobPositionEnum.Superadmin
            });
            builder.Entity<UserAccount>().HasData(new UserAccount
            {
                Id = REKTOR_ACCOUNT_ID,
                UserName = "rektor@fakultet.pl",
                NormalizedUserName = "REKTOR@FAKULTET.PL",
                Email = "rektor@fakultet.pl",
                NormalizedEmail = "REKTOR@FAKULTET.PL",
                EmailConfirmed = true,
                PasswordHash = hasher.HashPassword(null, "rektor123"),
                SecurityStamp = string.Empty,
                FirstName = "Michał",
                LastName = "Szczytny",
                NumerIndeksu = null,
                JobPosition = JobPositionEnum.Rektor
            });
            builder.Entity<UserAccount>().HasData(new UserAccount
            {
                Id = DZIEKAN_ACCOUNT_ID,
                UserName = "dziekan@fakultet.pl",
                NormalizedUserName = "DZIEKAN@FAKULTET.PL",
                Email = "dziekan@fakultet.pl",
                NormalizedEmail = "DZIEKAN@FAKULTET.PL",
                EmailConfirmed = true,
                PasswordHash = hasher.HashPassword(null, "dziekan123"),
                SecurityStamp = string.Empty,
                FirstName = "Jakub",
                LastName = "Marzyciel",
                NumerIndeksu = null,
                JobPosition = JobPositionEnum.Dziekan
            });
            builder.Entity<UserAccount>().HasData(new UserAccount
            {
                Id = PRACOWNIKDZIEKANATU_ACCOUNT_ID,
                UserName = "pracownikdziekanatu@fakultet.pl",
                NormalizedUserName = "PRACOWNIKDZIEKANATU@FAKULTET.PL",
                Email = "pracownikdziekanatu@fakultet.pl",
                NormalizedEmail = "PRACOWNIKDZIEKANATU@FAKULTET.PL",
                EmailConfirmed = true,
                PasswordHash = hasher.HashPassword(null, "pracownikdziekanatu123"),
                SecurityStamp = string.Empty,
                FirstName = "Patryk",
                LastName = "Kyrtap",
                NumerIndeksu = null,
                JobPosition = JobPositionEnum.PracownikDziekanatu
            });
            builder.Entity<UserAccount>().HasData(new UserAccount
            {
                Id = STUDENT_ACCOUNT_ID,
                UserName = "student@fakultet.pl",
                NormalizedUserName = "STUDENT@FAKULTET.pl",
                Email = "student@fakultet.pl",
                NormalizedEmail = "STUDENT@FAKULTET.pl",
                EmailConfirmed = true,
                PasswordHash = hasher.HashPassword(null, "student123"),
                SecurityStamp = string.Empty,
                FirstName = "Pilny",
                LastName = "Student",
                NumerIndeksu = "123456",
                JobPosition = JobPositionEnum.Student 
            });
            

            // seed relacji kont i ról
            builder.Entity<IdentityUserRole<string>>().HasData(new IdentityUserRole<string>
            {
                RoleId = STUDENT_ROLE_ID,
                UserId = STUDENT_ACCOUNT_ID
            });
            builder.Entity<IdentityUserRole<string>>().HasData(new IdentityUserRole<string>
            {
                RoleId = SUPERADMIN_ROLE_ID,
                UserId = SUPERADMIN_ACCOUNT_ID
            });
            builder.Entity<IdentityUserRole<string>>().HasData(new IdentityUserRole<string>
            {
                RoleId = ADMIN_ROLE_ID,
                UserId = REKTOR_ACCOUNT_ID
            });
            builder.Entity<IdentityUserRole<string>>().HasData(new IdentityUserRole<string>
            {
                RoleId = ADMIN_ROLE_ID,
                UserId = DZIEKAN_ACCOUNT_ID
            });
            builder.Entity<IdentityUserRole<string>>().HasData(new IdentityUserRole<string>
            {
                RoleId = MODERATOR_ROLE_ID,
                UserId = PRACOWNIKDZIEKANATU_ACCOUNT_ID
            });
            

            builder.Entity<DocumentType>().HasData(new DocumentType
            {
                Id = 1,
                Name = "Stypendia",
                AllowedToSendUserAccount = JobPositionEnum.Student,
                AcceptingUserAccount = JobPositionEnum.Dziekan,
                VerifyingUserAccount = JobPositionEnum.PracownikDziekanatu
            });
            builder.Entity<DocumentType>().HasData(new DocumentType
            {
                Id = 2,
                Name = "Warunki",
                AllowedToSendUserAccount = JobPositionEnum.Student,
                AcceptingUserAccount = JobPositionEnum.Dziekan,
                VerifyingUserAccount = JobPositionEnum.PracownikDziekanatu
            });
            builder.Entity<DocumentType>().HasData(new DocumentType
            {
                Id = 3,
                Name = "Urlop dziekański",
                AllowedToSendUserAccount = JobPositionEnum.Student,
                AcceptingUserAccount = JobPositionEnum.Dziekan,
                VerifyingUserAccount = JobPositionEnum.PracownikDziekanatu
            });
            builder.Entity<DocumentType>().HasData(new DocumentType
            {
                Id = 4,
                Name = "Podanie o dyplom",
                AllowedToSendUserAccount = JobPositionEnum.Student,
                AcceptingUserAccount = JobPositionEnum.Dziekan,
                VerifyingUserAccount = JobPositionEnum.PracownikDziekanatu
            });
            builder.Entity<DocumentType>().HasData(new DocumentType
            {
                Id = 5,
                Name = "Wypis z uczelni",
                AllowedToSendUserAccount = JobPositionEnum.Student,
                AcceptingUserAccount = JobPositionEnum.Dziekan,
                VerifyingUserAccount = JobPositionEnum.PracownikDziekanatu
            });
            builder.Entity<DocumentType>().HasData(new DocumentType
            {
                Id = 6,
                Name = "Zmiana kierunku studiów",
                AllowedToSendUserAccount = JobPositionEnum.Student,
                AcceptingUserAccount = JobPositionEnum.Rektor,
                VerifyingUserAccount = JobPositionEnum.PracownikDziekanatu
            });
            builder.Entity<DocumentType>().HasData(new DocumentType
            {
                Id = 7,
                Name = "Zmiana terminu pracy dyplomowej",
                AllowedToSendUserAccount = JobPositionEnum.Student,
                AcceptingUserAccount = JobPositionEnum.Rektor,
                VerifyingUserAccount = JobPositionEnum.PracownikDziekanatu
            });
            builder.Entity<DocumentType>().HasData(new DocumentType
            {
                Id = 8,
                Name = "Ponowne powtarzanie semestru",
                AllowedToSendUserAccount = JobPositionEnum.Student,
                AcceptingUserAccount = JobPositionEnum.Rektor,
                VerifyingUserAccount = JobPositionEnum.PracownikDziekanatu
            });
            #endregion
        }
    }
}
