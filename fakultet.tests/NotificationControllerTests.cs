﻿using System.Collections.Generic;
using fakultet.api.Controllers;
using fakultet.core.DTO.Returned;
using fakultet.core.Responses;
using fakultet.core.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Xunit;

namespace fakultet.tests
{
    public class NotificationControllerTests
    {
        private readonly Mock<INotificationService> _notificationServiceMock;

        public NotificationControllerTests()
        {
            _notificationServiceMock = new Mock<INotificationService>();
        }

        [Fact]
        public void GetNotificationsByUser_WhenCalled_ReturnsOkResult()
        {
            // arrange
            var notifications = new List<NotificationReturnDto>
            {
                new NotificationReturnDto {NotificationId = 1, Content = "Glowna tresc", Read = false},
                new NotificationReturnDto {NotificationId = 2, Content = "Glowna tresc", Read = false},
                new NotificationReturnDto {NotificationId = 3, Content = "Glowna tresc", Read = false},
            };

            var exampleResponse = new NotificationListResponse(notifications);
            _notificationServiceMock
                .Setup(s => s.GetNotificationsByUser())
                .Returns(exampleResponse);

            var controller = new NotificationController(_notificationServiceMock.Object);

            // act
            var result = controller.GetNotificationsByUser();

            // assert
            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public void GetNotificationsByUser_WhenCalled_ReturnsAllItems()
        {
            // arrange
            var notifications = new List<NotificationReturnDto>
            {
                new NotificationReturnDto {NotificationId = 1, Content = "Glowna tresc", Read = false},
                new NotificationReturnDto {NotificationId = 2, Content = "Glowna tresc", Read = false},
                new NotificationReturnDto {NotificationId = 3, Content = "Glowna tresc", Read = false},
            };

            var exampleResponse = new NotificationListResponse(notifications);
            _notificationServiceMock
                .Setup(s => s.GetNotificationsByUser())
                .Returns(exampleResponse);

            var controller = new NotificationController(_notificationServiceMock.Object);

            // act
            var result = controller.GetNotificationsByUser();
            var okResult = result as OkObjectResult;

            // assert
            var notificationsReturned = okResult?.Value;
            Assert.IsType<List<NotificationReturnDto>>(notificationsReturned);
            Assert.Equal(3, (notificationsReturned as List<NotificationReturnDto>)?.Count);
        }

        [Fact]
        public void MarkNotificationAsRead_ExistingIdPassed_ReturnsNoContentResponse()
        {
            // arrange
            int notificationId = 10;
            var errors = new Dictionary<string, string[]>();

            var exampleResponse = new Response(true, errors);
            _notificationServiceMock
                .Setup(s => s.MarkSingleNotificationAsRead(notificationId))
                .Returns(exampleResponse);

            var controller = new NotificationController(_notificationServiceMock.Object);

            // act
            var result = controller.MarkNotificationAsRead(notificationId);

            // assert
            Assert.IsType<NoContentResult>(result);
        }

        [Fact]
        public void MarkNotificationAsRead_UnknownIdPassed_ReturnsBadRequest()
        {
            // arrange
            var errors = new Dictionary<string, string[]>();
            errors.Add("DokumentTyp", new[] { "No jakis tam blad tutaj." });
            int notificationId = 10;

            var exampleResponse = new Response(false, errors);
            _notificationServiceMock
                .Setup(s => s.MarkSingleNotificationAsRead(notificationId))
                .Returns(exampleResponse);
            var controller = new NotificationController(_notificationServiceMock.Object);

            // act
            var result = controller.MarkNotificationAsRead(notificationId);

            // assert
            Assert.IsType<BadRequestObjectResult>(result);
        }
    }
}
