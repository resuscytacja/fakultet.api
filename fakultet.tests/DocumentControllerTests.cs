﻿using System.Collections.Generic;
using fakultet.api.Controllers;
using fakultet.common;
using fakultet.core.DTO.Requested;
using fakultet.core.DTO.Returned;
using fakultet.core.Responses;
using fakultet.core.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Xunit;

namespace fakultet.tests
{
    public class DocumentControllerTests
    {
        private readonly Mock<IDocumentService> _documentServiceMock;

        public DocumentControllerTests()
        {
            _documentServiceMock = new Mock<IDocumentService>();
        }

        [Fact]
        public void GetDetailsDocument_ExistingIdPassed_ReturnsOkResponse()
        {
            // arrange
            const int documentId = 420;
            var exampleDocumentObject = new DocumentReturnDto {DocumentId = documentId, Title = "Wniosek o pokój na świecie!"};
            var exampleResponse = new DocumentResponse(exampleDocumentObject);
            _documentServiceMock
                .Setup(s => s.GetDetailsDocument(documentId))
                .Returns(exampleResponse);
            var controller = new DocumentController(_documentServiceMock.Object);
            
            // act
            var result = controller.GetDetailsDocument(documentId);

            // assert
            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public void GetDetailsDocument_UnknownIdPassed_ReturnsBadRequest()
        {
            // arrange
            var errors = new Dictionary<string, string[]>();
            errors.Add("Dokument", new [] {"Nie ma dokumentu o takim ID"});

            var exampleResponse = new DocumentResponse(errors);
            _documentServiceMock
                .Setup(s => s.GetDetailsDocument(1234))
                .Returns(exampleResponse);
            var controller = new DocumentController(_documentServiceMock.Object);

            // act
            var result = controller.GetDetailsDocument(1234);

            // assert
            Assert.IsType<BadRequestObjectResult>(result);
        }

        [Fact]
        public void GetDetailsDocument_ExistingIdPassed_ReturnsCorrectObject()
        {
            // arrange
            const int documentId = 420;
            var exampleDocumentObject = new DocumentReturnDto { DocumentId = documentId, Title = "Wniosek o pokój na świecie!" };
            var exampleResponse = new DocumentResponse(exampleDocumentObject);
            _documentServiceMock
                .Setup(s => s.GetDetailsDocument(documentId))
                .Returns(exampleResponse);
            var controller = new DocumentController(_documentServiceMock.Object);

            // act
            var result = controller.GetDetailsDocument(documentId);
            var okResult = result as OkObjectResult;

            // assert
            Assert.IsType<DocumentReturnDto>(okResult?.Value);
            Assert.Equal(exampleDocumentObject.Title, (okResult.Value as DocumentReturnDto)?.Title);
        }

        [Fact]
        public void GetDocumentList_WhenCalled_ReturnsOkResult()
        {
            // arrange
            var documents = new List<DocumentToListReturnDto>
            {
                new DocumentToListReturnDto {DocumentId = 1, Title = "Nie wiem czy tak to się robi"},
            };
            var documentsListReturnDto = new DocumentListReturnDto{ Documents =  documents };
            var exampleResponse = new DocumentListResponse(documentsListReturnDto);
            _documentServiceMock
                .Setup(s => s.GetDocumentList("Id", "desc"))
                .Returns(exampleResponse);

            var controller = new DocumentController(_documentServiceMock.Object);

            // act
            var result = controller.GetDocumentList();

            // assert
            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public void GetDocumentList_WhenCalled_ReturnsAllItems()
        {
            // arrange
            var documents = new List<DocumentToListReturnDto>
            {
                new DocumentToListReturnDto {DocumentId = 1, Title = "Nie wiem czy tak to się robi"},
                new DocumentToListReturnDto {DocumentId = 2, Title = "Nie wiem czy tak to się robi"},
                new DocumentToListReturnDto {DocumentId = 3, Title = "Nie wiem czy tak to się robi"},
            };
            var documentsListReturnDto = new DocumentListReturnDto { Documents = documents };
            var exampleResponse = new DocumentListResponse(documentsListReturnDto);
            _documentServiceMock
                .Setup(s => s.GetDocumentList("Id", "desc"))
                .Returns(exampleResponse);

            var controller = new DocumentController(_documentServiceMock.Object);

            // act
            var result = controller.GetDocumentList();
            var okResult = result as OkObjectResult;

            // assert
            Assert.IsType<DocumentListResponse>(okResult?.Value);
            var documentsReturned = okResult?.Value;
            Assert.Equal(3, (documentsReturned as DocumentListResponse)?.Documents.Documents.Count);
        }

        [Fact]
        public void AddDocument_InvalidObjectPassed_ReturnsBadRequest()
        {
            // arrange
            var errors = new Dictionary<string, string[]>();
            errors.Add("Dokument", new[] { "No jakis tam blad tutaj." });
            var documentMissingTitle = new SaveDocumentDto();

            var exampleResponse = new DocumentResponse(errors);
            _documentServiceMock
                .Setup(s => s.AddDocument(documentMissingTitle))
                .Returns(exampleResponse);
            var controller = new DocumentController(_documentServiceMock.Object);

            // act
            var result = controller.AddDocument(documentMissingTitle);

            // assert
            Assert.IsType<BadRequestObjectResult>(result);
        }

        [Fact]
        public void AddDocument_ValidObjectPassed_ReturnsOkResult()
        {
            // arrange
            var validDocumentSave = new SaveDocumentDto { Title = "Tytul Dokumentu" };
            var documentReturnDto = new DocumentReturnDto {Title = "Tytul Dokumentu"};

            var exampleResponse = new DocumentResponse(documentReturnDto);
            _documentServiceMock
                .Setup(s => s.AddDocument(validDocumentSave))
                .Returns(exampleResponse);
            var controller = new DocumentController(_documentServiceMock.Object);

            // act
            var result = controller.AddDocument(validDocumentSave);

            // assert
            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public void AddDocument_ValidObjectPassed_ReturnsCreatedObject()
        {
            // arrange
            var validDocumentSave = new SaveDocumentDto { Title = "Tytul Dokumentu" };
            var documentReturnDto = new DocumentReturnDto { Title = "Tytul Dokumentu" };

            var exampleResponse = new DocumentResponse(documentReturnDto);
            _documentServiceMock
                .Setup(s => s.AddDocument(validDocumentSave))
                .Returns(exampleResponse);
            var controller = new DocumentController(_documentServiceMock.Object);


            // act
            var result = controller.AddDocument(validDocumentSave);
            var okResult = result as OkObjectResult;

            // assert
            Assert.IsType<DocumentReturnDto>(okResult?.Value);
            Assert.Equal(validDocumentSave.Title, (okResult.Value as DocumentReturnDto)?.Title);
        }

        [Fact]
        public void AddDocumentType_InvalidObjectPassed_ReturnsBadRequest()
        {
            // arrange
            var errors = new Dictionary<string, string[]>();
            errors.Add("DokumentTyp", new[] { "No jakis tam blad tutaj." });
            var documentTypeMissingName = new SaveDocumentTypeDto();

            var exampleResponse = new DocumentResponse(errors);
            _documentServiceMock
                .Setup(s => s.AddDocumentType(documentTypeMissingName))
                .Returns(exampleResponse);
            var controller = new DocumentController(_documentServiceMock.Object);

            // act
            var result = controller.AddDocumentType(documentTypeMissingName);

            // assert
            Assert.IsType<BadRequestObjectResult>(result);
        }

        [Fact]
        public void AddDocumentType_ValidObjectPassed_ReturnsNoContentResult()
        {
            // arrange
            var errors = new Dictionary<string, string[]>();
            var validDocumentTypeSave = new SaveDocumentTypeDto() { Name = "Tytul typu dokumentu" };

            var exampleResponse = new Response(true, errors);
            _documentServiceMock
                .Setup(s => s.AddDocumentType(validDocumentTypeSave))
                .Returns(exampleResponse);
            var controller = new DocumentController(_documentServiceMock.Object);

            // act
            var result = controller.AddDocumentType(validDocumentTypeSave);

            // assert
            Assert.IsType<NoContentResult>(result);
        }

        [Fact]
        public void VerificationDocument_ValidDataPassed_ReturnsNoContentResult()
        {
            // arrange
            var errors = new Dictionary<string, string[]>();
            int documentId = 10;
            var validModelDto = new EditModStatusDto {Reason = null, Status = DocumentStatusEnum.Verified};

            var exampleResponse = new Response(true, errors);
            _documentServiceMock
                .Setup(s => s.VerificationDocument(documentId, validModelDto))
                .Returns(exampleResponse);
            var controller = new DocumentController(_documentServiceMock.Object);

            // act
            var result = controller.VerificationDocument(documentId, validModelDto);

            // assert
            Assert.IsType<NoContentResult>(result);
        }

        [Fact]
        public void VerificationDocument_InvalidDataPassed_ReturnsBadRequestResult()
        {
            // arrange
            var errors = new Dictionary<string, string[]>();
            errors.Add("DokumentTyp", new[] { "No jakis tam blad tutaj." });
            int documentId = 10;
            // not accepted DocumentStatusEnum
            var invalidModelData = new EditModStatusDto { Reason = null, Status = DocumentStatusEnum.Accepted }; 

            var exampleResponse = new Response(false, errors);
            _documentServiceMock
                .Setup(s => s.VerificationDocument(documentId, invalidModelData))
                .Returns(exampleResponse);
            var controller = new DocumentController(_documentServiceMock.Object);

            // act
            var result = controller.VerificationDocument(documentId, invalidModelData);

            // assert
            Assert.IsType<BadRequestObjectResult>(result);
        }
    }
}
