﻿using System.IO;
using AutoMapper;
using fakultet.core.AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;

namespace fakultet.api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        private IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSqlServerContext(Configuration);
            services.AddJwtAuthentication(Configuration);
            services.AddIdentity();

            services.AddCorsPolicy("CorsPolicy");
            services.AddServicesLayerDependencyInjection();
            services.AddAutoMapper(typeof(ResourceToModelProfile));
            services.AddCustomMvc();
            services.AddSwaggerGenerator();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseCors("CorsPolicy");
            app.UseSwagger();

            app.UseSwaggerUI(options =>
            {
                options.SwaggerEndpoint("/swagger/v1/swagger.json", "Fakultet.API V1");
                options.RoutePrefix = string.Empty;
            });

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseStaticFiles(new StaticFileOptions
            {
                FileProvider = new PhysicalFileProvider(Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\file")),
                RequestPath = "/file"
            });

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
