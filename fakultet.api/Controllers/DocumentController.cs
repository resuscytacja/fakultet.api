﻿using System.Collections.Generic;
using fakultet.core.DTO.Requested;
using fakultet.core.DTO.Returned;
using fakultet.core.Services.Interfaces;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace fakultet.api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DocumentController : ControllerBase
    {
        private readonly IDocumentService _documentService;

        public DocumentController(IDocumentService documentService)
        {
            _documentService = documentService;
        }

        /// <summary>Zwraca listę typów(kategorii) dokumentów</summary>
        /// <returns>Lista typów dokumentów (kategorii)</returns>
        /// <response code="200">Lista typów dokumentów (id, nazwa, JobPosition odbiorcy)</response>
        /// <response code="400">Jeśli są jakieś błędy</response>
        [ProducesResponseType(typeof(DocumentTypeListReturnDto), 200)]
        [ProducesResponseType(typeof(IDictionary<string, string[]>), 400)]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpGet("all_document_types")]
        public IActionResult GetAllDocumentTypes()
        {
            var result = _documentService.GetAllDocumentTypes();

            if (!result.Success) return BadRequest(result.Message);

            return Ok(result.DocumentTypeList);
        }

        /// <summary>SUPERADMIN dodaje nowe typy dokumentów.</summary>
        /// <remarks>
        /// Name - nazwa typu dokumentów (np Wniosek o spokój wieczny)
        /// AcceptingUserAccount - do wyboru pomiędzy PracownikDziekanatu/Dziekan/Rektor/Student
        /// AllowedToSendUserAccount - jak wyzej
        /// VerifyingUserAccount - jak wyzej, jeszcze wyżej!
        /// </remarks>
        /// <returns>NoContent</returns>
        /// <response code="204">Jeśli bez problemu dodano nowy typ dokumentów.</response>
        /// <response code="400">Jeśli są jakieś błędy</response>
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(IDictionary<string, string[]>), 400)]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Superadmin")]
        [HttpPost("add_document_type")]
        public IActionResult AddDocumentType([FromBody] SaveDocumentTypeDto model)
        {
            var result = _documentService.AddDocumentType(model);

            if (!result.Success) return BadRequest(result.Message);

            return NoContent();
        }

        /// <summary>Zwraca szczegółowe informacje o dokumencie</summary>
        /// <response code="200">Informacje o dokumencie</response>
        /// <response code="400">Jeśli są jakieś błędy</response>
        [ProducesResponseType(typeof(DocumentReturnDto), 200)]
        [ProducesResponseType(typeof(IDictionary<string, string[]>), 400)]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpGet("{id}")]
        public IActionResult GetDetailsDocument(int id)
        {
            var result = _documentService.GetDetailsDocument(id);

            if (!result.Success) return BadRequest(result.Message);

            return Ok(result.Document);
        }

        /// <summary>Dodaje nowy dokument</summary>
        /// <response code="200">Jeśli bez problemu dodamy nowe ogloszenie</response>
        /// <response code="400">Jeśli sa jakies bledy, nie wypełniono wszystkich pol</response>
        [ProducesResponseType(typeof(DocumentReturnDto), 200)]
        [ProducesResponseType(typeof(IDictionary<string, string[]>), 400)]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [Consumes("multipart/form-data")]
        [HttpPost("add_document")]
        public IActionResult AddDocument([FromForm] SaveDocumentDto documentSaveDto)
        {
            var result = _documentService.AddDocument(documentSaveDto);

            if (!result.Success) return BadRequest(result.Message);

            return Ok(result.Document);
        }

        /// <summary>Zmienia staus na Verified lub UnVerified</summary>
        /// <response code="200">Informacje o dokumencie</response>
        /// <response code="400">Jeśli są jakieś błędy</response>
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(IDictionary<string, string[]>), 400)]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpPut("verification/{id}")]
        public IActionResult VerificationDocument(int id, [FromBody] EditModStatusDto status)
        {
            var result = _documentService.VerificationDocument(id,status);

            if (!result.Success) return BadRequest(result.Message);

            return NoContent();
        }

        /// <summary>Zmienia staus na Accepted lub Cancelled lub Return</summary>
        /// <response code="200">Informacje o dokumencie</response>
        /// <response code="400">Jeśli są jakieś błędy</response>
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(IDictionary<string, string[]>), 400)]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpPut("consideration/{id}")]
        public IActionResult ConsiderationDocument(int id, [FromBody] EditModStatusDto status)
        {
            var result = _documentService.ConsiderationDocument(id, status);

            if (!result.Success) return BadRequest(result.Message);

            return NoContent();
        }

        /// <summary>Wyswietla liste dokumentow</summary>
        /// <response code="200">Wyswietla liste dokumentow dla studenta,
        /// listy do weryfikacji/odrzucenia dla pracownikow dziekanatu,
        /// listy do akceptacji/odrzucenia/cofniecia dla rektora lub dziekana
        /// na podstawie tokenu</response>
        /// <param name="orderBy">Po jakich danych sortowac (DocumentTypeName, Status, Title, SendTime)</param>
        /// <param name="orderType">Sposob sortowania (asc - od najwiekszego do najmniejszego, desc - odwrotnie)</param>
        /// <response code="400">Jeśli są jakieś błędy</response>
        [ProducesResponseType(typeof(DocumentListReturnDto), 200)]
        [ProducesResponseType(typeof(IDictionary<string, string[]>), 400)]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpGet("documentList")]
        public IActionResult GetDocumentList(string orderBy = "Id", string orderType = "desc")
        {
            var result = _documentService.GetDocumentList(orderBy, orderType);

            if (!result.Success) return BadRequest(result.Message);

            return Ok(result);
        }

        /// <summary>Edytuje istnijeacy dokument dokument</summary>
        /// <response code="200">Jeśli bez problemu dodamy nowe ogloszenie</response>
        /// <response code="400">Jeśli sa jakies bledy, nie wypełniono wszystkich pol</response>
        [ProducesResponseType(typeof(DocumentReturnDto), 200)]
        [ProducesResponseType(typeof(IDictionary<string, string[]>), 400)]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [Consumes("multipart/form-data")]
        [HttpPost("edit/{id}")]
        public IActionResult EditDocument(int id,[FromForm] EditDocumentDto EditDocuemtModel)
        {
            var result = _documentService.EditDocument(id, EditDocuemtModel);

            if (!result.Success) return BadRequest(result.Message);

            return Ok(result.Document);
        }
    }
}