﻿using System.Collections.Generic;
using System.Threading.Tasks;
using fakultet.core.DTO.Requested;
using fakultet.core.DTO.Returned;
using fakultet.core.Services.Interfaces;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace fakultet.api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly IAccountService _accountService;

        public AccountController(IAccountService accountService)
        {
            _accountService = accountService;
        }

        
        /// <summary>Student loguje się na konto (emailem i hasłem).</summary>
        /// <returns>NoContent</returns>
        /// <response code="200">JWT - jeśli student się poprawnie zalogował</response>
        /// <response code="400">Jeśli są jakieś błędy</response>
        [ProducesResponseType(typeof(JwtTokenReturnDto), 200)]
        [ProducesResponseType(typeof(IDictionary<string, string[]>), 400)]
        [HttpPost("login")]
        public async Task<IActionResult> LoginStudent([FromBody] LoginStudentDto model)
        {
            var result = await _accountService.LoginStudentAsync(model);

            if (!result.Success) return BadRequest(result.Message);

            return Ok(result.Token);
        }

        /// <summary>Zmiana hasla uzytkownika zalogowanego.</summary>
        /// <returns>Nowy token</returns>
        /// <response code="200">JWT - jeśli student się poprawnie zalogował</response>
        /// <response code="400">Jeśli są jakieś błędy</response>
        [ProducesResponseType(typeof(JwtTokenReturnDto), 200)]
        [ProducesResponseType(typeof(IDictionary<string, string[]>), 400)]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpPut("editPassword")]
        public async Task<IActionResult> EditAccountPassword([FromBody] EditAccountPasswordDto model)
        {
            var result = await _accountService.EditAccountPassword(model);

            if (!result.Success) return BadRequest(result.Message);

            return Ok(result.Token);

        }
    }
}