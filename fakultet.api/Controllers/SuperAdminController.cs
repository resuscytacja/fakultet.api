﻿using System.Collections.Generic;
using System.Threading.Tasks;
using fakultet.core.DTO.Requested;
using fakultet.core.DTO.Returned;
using fakultet.core.Services.Interfaces;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace fakultet.api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SuperAdminController : ControllerBase
    {
        private readonly ISuperAdminService _superAdminService;

        public SuperAdminController(ISuperAdminService superAdminService)
        {
            _superAdminService = superAdminService;
        }


        /// <summary>Administrator tworzy nowe konto użytkownika (admin/moderator/student).</summary>
        /// <remarks>
        /// Role - do wyboru pomiędzy Admin/Moderator/Student
        /// JobPosition - do wyboru pomiędzy PracownikDziekanatu/Dziekan/Rektor/Student
        /// NumerIndeksu - pusty jeśli nie dodajemy Studenta
        /// </remarks>
        /// <returns>NoContent</returns>
        /// <response code="204">Jeśli bez problemu dodano nowego użytkownika.</response>
        /// <response code="400">Jeśli są jakieś błędy</response>
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(IDictionary<string, string[]>), 400)]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Superadmin")]
        [HttpPost("add_account")]
        public async Task<IActionResult> RegisterAccount([FromBody] AddAccountDto model)
        {
            var result = await _superAdminService.AddAccountAsync(model);

            if (!result.Success) return BadRequest(result.Message);

            return NoContent();
        }

        
        /// <summary>Superadmin loguje się na konto (username i hasłem).</summary>
        /// <returns>NoContent</returns>
        /// <response code="200">JWT - jeśli superadmin się poprawnie zalogował</response>
        /// <response code="400">Jeśli są jakieś błędy</response>
        [ProducesResponseType(typeof(JwtTokenReturnDto), 200)]
        [ProducesResponseType(typeof(IDictionary<string, string[]>), 400)]
        [HttpPost("login")]
        public async Task<IActionResult> LoginAdministrator([FromBody] LoginSuperAdminDto model)
        {
            var result = await _superAdminService.LoginSuperAdminAsync(model);

            if (!result.Success) return BadRequest(result.Message);

            return Ok(result.Token);
        }
    }
}