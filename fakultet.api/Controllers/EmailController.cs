﻿using fakultet.core.DTO.Requested;
using fakultet.core.Services.Interfaces;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace fakultet.api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmailController : ControllerBase
    {
        private readonly IEmailSendService _emailSendService;

        public EmailController(IEmailSendService emailService)
        {
            _emailSendService = emailService;
        }

        /// <summary>Wysyla maila do uzytkownika nie zalogowanego.</summary>
        /// <returns>NoContent</returns>
        /// <response code="204">Jeśli bez problemu wyslano maila.</response>
        /// <response code="400">Jeśli są jakieś błędy</response>
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(IDictionary<string, string[]>), 400)]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpPost("send_email_to_unregistered")]
        public IActionResult AddDocumentType([FromForm] SendEmailToUnregisteredDto model)
        {
            var result = _emailSendService.SendEmailToUnregistered(model);

            if (!result.Success) return BadRequest(result.Message);

            return NoContent();

        }
    }
}
