﻿using System.Collections.Generic;
using fakultet.core.Responses;
using fakultet.core.Services.Interfaces;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace fakultet.api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NotificationController : ControllerBase
    {
        private readonly INotificationService _notificationsService;

        public NotificationController(INotificationService notificationsService)
        {
            _notificationsService = notificationsService;
        }

        /// <summary>
        ///     Zwraca listę powiadomień zalogowanego użytkownika
        /// </summary>
        /// <returns>Lista powiadomień zalogowanego użytkownika</returns>
        /// <response code="200">
        ///     Lista powiadomień zalogowanego użytkownika
        /// </response>
        /// <response code="400">
        ///     Jeśli są jakieś błędy
        /// </response>
        [ProducesResponseType(typeof(NotificationListResponse), 200)]
        [ProducesResponseType(typeof(IDictionary<string, string[]>), 400)]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpGet("my")] // api/notification/my
        public IActionResult GetNotificationsByUser()
        {
            var result = _notificationsService.GetNotificationsByUser();

            if (!result.Success) return BadRequest(result.Message);

            return Ok(result.Notifications);
        }


        /// <summary>
        ///     Oznacza powiadomienie jako przeczytane
        /// </summary>
        /// <returns>NoContent (204) lub 400 (błędy)</returns>
        /// <response code="204">
        /// </response>
        /// <response code="400">
        ///     Jeśli są jakieś błędy
        /// </response>
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(IDictionary<string, string[]>), 400)]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpPut("markasread/{id}")] // api/notification/markasread/{id}
        public IActionResult MarkNotificationAsRead(int id)
        {
            var result = _notificationsService.MarkSingleNotificationAsRead(id);

            if (!result.Success) return BadRequest(result.Message);

            return NoContent();
        }

        /// <summary>
        ///     Oznacza wszystkie powiadomienia użytkownika jako przeczytane
        /// </summary>
        /// <returns>NoContent (204) lub 400 (błędy)</returns>
        /// <response code="204">
        /// </response>
        /// <response code="400">
        ///     Jeśli są jakieś błędy
        /// </response>
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(IDictionary<string, string[]>), 400)]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpPut("markasread/all")] // api/notification/markasread/all
        public IActionResult MarkAllNotificationsAsRead()
        {
            var result = _notificationsService.MarkAllNotificationsAsRead();

            if (!result.Success) return BadRequest(result.Message);

            return NoContent();
        }
    }
}