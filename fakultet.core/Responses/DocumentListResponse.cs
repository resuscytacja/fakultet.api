﻿using fakultet.core.DTO.Returned;
using System;
using System.Collections.Generic;
using System.Text;

namespace fakultet.core.Responses
{
    public class DocumentListResponse : Response
    {
            public DocumentListReturnDto Documents { get; }

            private DocumentListResponse(bool success, Dictionary<string, string[]> message, DocumentListReturnDto documents)
        : base(success, message)
            {
                Documents = documents;
            }

            public DocumentListResponse(DocumentListReturnDto documents)
                : this(true, new Dictionary<string, string[]>(), documents)
            {
            }

            public DocumentListResponse(Dictionary<string, string[]> message)
                : this(false, message, null)
            {
            }
    }
}
