﻿using fakultet.core.DTO.Returned;
using System;
using System.Collections.Generic;
using System.Text;

namespace fakultet.core.Responses
{
    public class DocumentResponse : Response
    {
        public DocumentReturnDto Document { get; }

        private DocumentResponse(bool success, Dictionary<string, string[]> message, DocumentReturnDto document)
    : base(success, message)
        {
            Document = document;
        }

        public DocumentResponse(DocumentReturnDto document)
            : this(true, new Dictionary<string, string[]>(), document)
        {
        }

        public DocumentResponse(Dictionary<string, string[]> message)
            : this(false, message, null)
        {
        }

    }
}
