﻿using System.Collections.Generic;
using fakultet.core.DTO.Returned;

namespace fakultet.core.Responses
{
    public class AccountResponse : Response
    {
        public JwtTokenReturnDto Token { get; }

        private AccountResponse(bool success, Dictionary<string, string[]> message, JwtTokenReturnDto token)
            : base(success, message)
        {
            Token = token;
        }

        public AccountResponse(JwtTokenReturnDto token)
            : this(true, new Dictionary<string, string[]>(), token)
        {
        }

        public AccountResponse(Dictionary<string, string[]> message)
            : this(false, message, null)
        {
        }
    }
}
