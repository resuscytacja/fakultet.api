﻿using System.Collections.Generic;
using fakultet.core.DTO.Returned;

namespace fakultet.core.Responses
{
    public class NotificationListResponse : Response
    {
        public List<NotificationReturnDto> Notifications { get; }

        private NotificationListResponse(bool success, Dictionary<string, string[]> message, List<NotificationReturnDto> notifications)
            : base(success, message)
        {
            Notifications = notifications;
        }

        public NotificationListResponse(List<NotificationReturnDto> notifications)
            : this(true, new Dictionary<string, string[]>(), notifications)
        {

        }

        public NotificationListResponse(Dictionary<string, string[]> message)
            : this(false, message, null)
        {
        }
    }
}
