﻿using System.Collections.Generic;
using fakultet.core.DTO.Returned;

namespace fakultet.core.Responses
{
    public class DocumentTypeListResponse : Response
    {
        public DocumentTypeListReturnDto DocumentTypeList { get; }

        private DocumentTypeListResponse(bool success, Dictionary<string, string[]> message, DocumentTypeListReturnDto documentTypeList)
            : base(success, message)
        {
            DocumentTypeList = documentTypeList;
        }

        public DocumentTypeListResponse(DocumentTypeListReturnDto reservation)
            : this(true, new Dictionary<string, string[]>(), reservation)
        {

        }

        public DocumentTypeListResponse(Dictionary<string, string[]> message)
            : this(false, message, null)
        {
        }
    }
}
