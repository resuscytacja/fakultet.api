﻿using System.Collections.Generic;

namespace fakultet.core.Responses
{
    public class Response
    {
        public bool Success { get; }
        public Dictionary<string, string[]> Message { get; }

        public Response(bool success, Dictionary<string, string[]> message)
        {
            Success = success;
            Message = message;
        }
    }
}
