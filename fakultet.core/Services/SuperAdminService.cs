﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using fakultet.common;
using fakultet.core.DTO.Requested;
using fakultet.core.DTO.Returned;
using fakultet.core.Responses;
using fakultet.core.Services.Interfaces;
using fakultet.DAL;
using fakultet.DAL.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;

namespace fakultet.core.Services
{
    public class SuperAdminService : ISuperAdminService
    {
        private readonly IMapper _mapper;
        private readonly AppDbContext _context;
        private readonly SignInManager<UserAccount> _signInManager;
        private readonly UserManager<UserAccount> _userManager;
        private readonly IConfiguration _configuration;
        private readonly IEmailSendService _emailSendService;

        public SuperAdminService(IMapper mapper, AppDbContext context, UserManager<UserAccount> userManager, SignInManager<UserAccount> signInManager,
            IConfiguration configuration, IEmailSendService emailSendService)
        {
            _context = context;
            _mapper = mapper;
            _userManager = userManager;
            _signInManager = signInManager;
            _configuration = configuration;
            _emailSendService = emailSendService;
        }

        
        public async Task<Response> AddAccountAsync(AddAccountDto model)
        {
            var errors = new Dictionary<string, string[]>();
            var user = _mapper.Map<AddAccountDto, UserAccount>(model);

            // TODO: używać Enum zamiast hard-coded nazwy ról użytkowników
            // sprawdza czy podany numer indeksu jest unikalny w bazie
            if (model.Role.ToLower() == "student")
            {
                var userUniqueNumerIndeksu =
                    _context.UserAccounts.SingleOrDefault(u => u.NumerIndeksu == model.NumerIndeksu);
                if (userUniqueNumerIndeksu != null)
                {
                    errors.Add("Konto", new[] { "Student o podanym Numerze Indeksu już istnieje" });
                    return new Response(false, errors);
                }
            }

            string randomPassword = PasswordGenerator.GetPassword(7);
            var result = await _userManager.CreateAsync(user, randomPassword);
            if (!result.Succeeded)
            {
                foreach (var error in result.Errors) errors.Add(error.Code, new[] { error.Description });
                return new Response(false, errors);
            }

            _context.SaveChanges();
            await _userManager.AddToRoleAsync(user, model.Role);

            _emailSendService.SendRegistrationEmail(user.Email,randomPassword, user.FirstName, user.LastName);
            return new Response(true, errors);
        }

        
        public async Task<AccountResponse> LoginSuperAdminAsync(LoginSuperAdminDto model)
        {
            var errors = new Dictionary<string, string[]>();
            var result = await _signInManager.PasswordSignInAsync(model.Username, model.Password, false, false);
            if (!result.Succeeded)
            {
                errors.Add("Konto", new[] { "Nie udało się zalogować" });
                return new AccountResponse(errors);
            }

            var appUser = _userManager.Users.SingleOrDefault(r => r.UserName == model.Username);
            var userRoles = await _userManager.GetRolesAsync(appUser);

            var response = new JwtTokenReturnDto()
            {
                Token = Token.GenerateJwtToken(appUser, userRoles, _configuration)
            };

            return new AccountResponse(response);
        }
    }
}
