﻿using fakultet.core.DTO.Requested;
using fakultet.core.Services.Interfaces;
using MailKit.Net.Smtp;
using MailKit;
using MimeKit;
using MailKit.Security;
using fakultet.core.Responses;
using System.Collections.Generic;
using fakultet.DAL.Models;
using fakultet.DAL;
using Microsoft.AspNetCore.Http;
using System.Linq;
using System.Security.Claims;
using System.IO;
using fakultet.common;

namespace fakultet.core.Services
{
    public class EmailSendService : IEmailSendService
    {
        private readonly AppDbContext _context;
        private readonly string _userEmail;

        public EmailSendService(AppDbContext context, IHttpContextAccessor httpContext)
        {
            _context = context;
            _userEmail = httpContext.HttpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier)?.Value;

        }


        public void SendRegistrationEmail(string email, string password, string FirstName, string LastName)
        {
            var message = new MimeMessage();

            message.From.Add(new MailboxAddress("No Reply", "emailitest576@gmail.com"));

            message.To.Add(new MailboxAddress(FirstName +" "+ LastName, email));

            message.Subject = "Zostałeś zarejestrowany w systemie - Fakultet Billenium";

            var builder = new BodyBuilder();

            builder.TextBody = @"Dzień dobry "+ FirstName +" "+ LastName +@",

zostałeś zarejestrowany w naszym systemie.

Twoje wygenerowane hasło to: "+ password +@"

Za pomocą tego hasła oraz adresu email, na który przyszła wiadomość, możesz zalogować się na naszej stronie głównej.

Pozdrawiamy
Zespół Resuscytacja
";
            message.Body = builder.ToMessageBody();

            using (var client = new SmtpClient())
            {
                client.ServerCertificateValidationCallback = (s, c, h, e) => true;

                client.Connect("smtp.gmail.com", 587, SecureSocketOptions.Auto);

                client.Authenticate("emailitest576@gmail.com", "emailtest");

                client.Send(message);

                client.Disconnect(true);

                client.Dispose();
            }
        }

        public Response SendEmailToUnregistered(SendEmailToUnregisteredDto model)
        {
            var errors = new Dictionary<string, string[]>();

            UserAccount user = _context.UserAccounts.FirstOrDefault(u => u.Email == _userEmail);
            if (user == null)
            {
                errors.Add("User", new[] { "Podane konto nie istnieje" });
                return new Response(false,errors);
            }

            var message = new MimeMessage();

            message.From.Add(new MailboxAddress("No Reply", "emailitest576@gmail.com"));

            message.To.Add(new MailboxAddress("", model.EmailRecipient));

            message.Subject = model.Title;

            var builder = new BodyBuilder();

            builder.TextBody = model.Content + @"





Odpowiedź wyślij na email:" + user.Email + @"

Pozdrawiamy
Zespół Resuscytacja";

            if (model.FileList != null)
            {
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    foreach (var File in model.FileList.ToList())
                    {
                        File.CopyTo(memoryStream);
                        builder.Attachments.Add(File.FileName, memoryStream.ToArray(), ContentType.Parse(File.ContentType));
                    }

                }
            }

            message.Body = builder.ToMessageBody();

            using (var client = new SmtpClient())
            {
                client.ServerCertificateValidationCallback = (s, c, h, e) => true;

                client.Connect("smtp.gmail.com", 587, SecureSocketOptions.Auto);

                client.Authenticate("emailitest576@gmail.com", "emailtest");

                client.Send(message);

                client.Disconnect(true);

                client.Dispose();
            }

            return new Response(true, errors);
        }

        public void SendNotificationEmail(Document document)
        {
            string status = null;
            string reason = null;

            if(document.Status == DocumentStatusEnum.Unverified)
            {
                status = "Niezweryfikowany";
                reason = document.UnverifiedReason;
            }
            else if(document.Status == DocumentStatusEnum.Accepted)
            {
                status = "Zaakceptowany";
            }
            else if(document.Status == DocumentStatusEnum.Cancelled)
            {
                status = "Odrzucony";
                reason = document.CancelledReason;
            }

            if (status != null)
            {
                var message = new MimeMessage();

                message.From.Add(new MailboxAddress("No Reply", "emailitest576@gmail.com"));

                message.To.Add(new MailboxAddress(document.CreatedBy.FirstName +" "+ document.CreatedBy.LastName, document.CreatedBy.Email));

                message.Subject = "Twój dokument otrzymał nowy status";

                var builder = new BodyBuilder();

                if (reason == null)
                {
                    builder.TextBody = @"Dzień dobry " + document.CreatedBy.FirstName + " " + document.CreatedBy.LastName + @",

Twój dokument o tytule: " + document.Title +@" zmienił status na " + status +@".


Pozdrawiamy
Zespół Resuscytacja
";
                }
                else
                {
                    builder.TextBody = @"Dzień dobry " + document.CreatedBy.FirstName + " " + document.CreatedBy.LastName + @",

Twój dokument o tytule: " + document.Title + @" zmienił status na " + status + @".
Powod odrzucenia: " + reason + @"

Pozdrawiamy
Zespół Resuscytacja
";
                }
                message.Body = builder.ToMessageBody();

                using (var client = new SmtpClient())
                {
                    client.ServerCertificateValidationCallback = (s, c, h, e) => true;

                    client.Connect("smtp.gmail.com", 587, SecureSocketOptions.Auto);

                    client.Authenticate("emailitest576@gmail.com", "emailtest");

                    client.Send(message);

                    client.Disconnect(true);

                    client.Dispose();
                }
            }

        }
    }
}
