﻿using AutoMapper;
using fakultet.core.DTO.Requested;
using fakultet.core.DTO.Returned;
using fakultet.core.Responses;
using fakultet.core.Services.Interfaces;
using fakultet.DAL;
using fakultet.DAL.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using fakultet.common;
using fakultet.core.Extensions;
using Microsoft.EntityFrameworkCore;

namespace fakultet.core.Services
{
    public class DocumentService : IDocumentService
    {
        private readonly AppDbContext _context;
        private readonly IFileService _fileService;
        private readonly IDocumentActivityService _documentActivityService;
        private readonly INotificationService _notificationService;
        private readonly IMapper _mapper;
        private readonly string _userEmail;
        private readonly string _jobPosition;
        private readonly IEmailSendService _emailSendService;

        public DocumentService(IMapper mapper, IHttpContextAccessor httpContext, AppDbContext context,
            IFileService fileService, IDocumentActivityService documentActivityService, INotificationService notificationService, IEmailSendService emailSendService)
        {
            _mapper = mapper;
            _fileService = fileService;
            _documentActivityService = documentActivityService;
            _userEmail = httpContext.HttpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier)?.Value;
            _jobPosition = httpContext.HttpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Actor)?.Value;
            _context = context;
            _notificationService = notificationService;
            _emailSendService = emailSendService;
        }

        public DocumentResponse AddDocument(SaveDocumentDto documentSaveDto)
        {
            var errors = new Dictionary<string, string[]>();

            var documentType = _context.DocumentTypes.SingleOrDefault(doc => doc.Id == documentSaveDto.DocumentTypeId);
            if (documentType == null)
            {
                errors.Add("DocumentType", new[] { "Podana kategoria nie istnieje" });
                return new DocumentResponse(errors);
            }

            var user = _context.UserAccounts.FirstOrDefault(u => u.Email == _userEmail);
            if (user == null)
            {
                errors.Add("User", new[] { "Podany uzytkownik nie istnieje" });
                return new DocumentResponse(errors);
            }

            var documentModel = _mapper.Map<SaveDocumentDto, Document>(documentSaveDto);
            documentModel.CreatedBy = user;
            documentModel.DocumentType = documentType;
            documentModel.SendTime = DateTime.Now;
            documentModel.Status = 0;
            documentModel.VerificationTime = null;
            documentModel.ConsiderationTime = null;

            var uploadedFileModels = new List<Files>();
            if (documentSaveDto.FileList == null)
            {
                errors.Add("DOkument", new[] { "Musisz wyslac co najmniej jeden dokument" });
                return new DocumentResponse(errors);
            }
            try
            {
                uploadedFileModels = _fileService.UploadFileToServer(documentSaveDto.FileList, documentModel);
            }
            catch (Exception ex)
            {
                errors.Add("File", new[] { ex.Message });
                return new DocumentResponse(errors);
            }
            documentModel.FileList = uploadedFileModels;
            try
            {
                _context.Files.AddRange(uploadedFileModels);
                _context.Documents.Add(documentModel);
                _context.SaveChanges();

                _documentActivityService.AddActivity(documentModel, user, DocumentActivityTypeEnum.Created);

                var documentReturnDto = _mapper.Map<Document, DocumentReturnDto>(documentModel);
                return new DocumentResponse(documentReturnDto);
            }
            catch (Exception ex)
            {
                errors.Add("UNEXPECTED PATRONUM", new[] { ex.Message });

                return new DocumentResponse(errors);
            }
        }

        public DocumentTypeListResponse GetAllDocumentTypes()
        {
            var errors = new Dictionary<string, string[]>();

            UserAccount user = _context.UserAccounts.FirstOrDefault(u => u.Email == _userEmail);
            if (user == null)
            {
                errors.Add("User", new[] { "Podane konto nie istnieje" });
                return new DocumentTypeListResponse(errors);
            }

            List<DocumentType> documentTypeList;
            if (_jobPosition == JobPositionEnum.Superadmin.ToString())
            {
                documentTypeList = _context.DocumentTypes.ToList();
            }
            else
            {
                documentTypeList = _context.DocumentTypes.Where(d => d.AllowedToSendUserAccount == user.JobPosition).ToList();
            }

            var documentTypeListReturnDto =
                _mapper.Map<List<DocumentType>, List<DocumentTypeReturnDto>>(documentTypeList);

            var listResponse = new DocumentTypeListReturnDto
            {
                DocumentTypeList = documentTypeListReturnDto
            };

            return new DocumentTypeListResponse(listResponse);
        }

        public DocumentResponse GetDetailsDocument(int id)
        {
            var errors = new Dictionary<string, string[]>();
            var user = _context.UserAccounts.FirstOrDefault(u => u.Email == _userEmail);
            if (user == null)
            {
                errors.Add("User", new[] { "Podane konto nie istnieje" });
                return new DocumentResponse(errors);
            }

            var document = _context.Documents
                .Include(a => a.DocumentType)
                .Include(a => a.FileList)
                .Include(a => a.ConsiderationBy)
                .Include(a => a.VerificationBy)
                .Include(a => a.Activities)
                    .ThenInclude(activities => activities.InitiatedBy)
                .SingleOrDefault(a => a.Id == id);


            if (_jobPosition == JobPositionEnum.Student.ToString())
            {
                if (document.CreatedBy.Id.ToString() != user.Id)
                {
                    errors.Add("User", new[] { "Nie jestes wlascicielem tego dokuemtu" });
                    return new DocumentResponse(errors);
                }
            }

            if (document == null)
            {
                errors.Add("Document", new[] { "Nie ma takiego dokumentu" });
                return new DocumentResponse(errors);
            }

            document.Activities = document.Activities.OrderByDescending(a => a.DocumentActivityId).ToList();
            var documentReturnModel = _mapper.Map<Document, DocumentReturnDto>(document);
            return new DocumentResponse(documentReturnModel);
        }

        public Response AddDocumentType(SaveDocumentTypeDto model)
        {
            var errors = new Dictionary<string, string[]>();

            UserAccount user = _context.UserAccounts.FirstOrDefault(u => u.Email == _userEmail);
            if (user == null)
            {
                errors.Add("User", new[] { "Podany uzytkownik nie istnieje" });
                return new Response(false, errors);
            }

            DocumentType uniqueDocumentType = _context.DocumentTypes.FirstOrDefault(u => u.Name == model.Name);
            if (uniqueDocumentType != null)
            {
                errors.Add("Typ Dokumentu", new[] { "Typ dokumentu o takiej nazwie już istnieje" });
                return new Response(false, errors);
            }

            DocumentType documentToSave = _mapper.Map<SaveDocumentTypeDto, DocumentType>(model);

            try
            {
                _context.DocumentTypes.Add(documentToSave);
                _context.SaveChanges();

                return new Response(true, errors);
            }
            catch (Exception ex)
            {
                errors.Add("UNEXPECTED PATRONUM", new[] { ex.Message });

                return new Response(false, errors);
            }
        }

        public Response VerificationDocument(int id, EditModStatusDto status)
        {
            var errors = new Dictionary<string, string[]>();
            var user = _context.UserAccounts.FirstOrDefault(u => u.Email == _userEmail);
            if (user == null)
            {
                errors.Add("User", new[] { "Podany uzytkownik nie istanieje" });
                return new Response(false, errors);
            }
            var document = _context.Documents.Include(a => a.DocumentType).Include(a => a.CreatedBy).FirstOrDefault(d => d.Id == id);

            if (document == null)
            {
                errors.Add("Dokument", new[] { "Nie ma takiego dokumentu" });
                return new Response(false, errors);
            }

            if (_jobPosition != document.DocumentType.VerifyingUserAccount.ToString())
            {
                errors.Add("JobPosition", new[] { "Nie masz akich uprawnien" });
                return new Response(false, errors);
            }

            if (document.Status != DocumentStatusEnum.Awaiting && document.Status != DocumentStatusEnum.Return)
            {
                errors.Add("Status", new[] { "Juz jest inny status niz awaiting lub return" });
                return new Response(false, errors);
            }

            if (status.Status != DocumentStatusEnum.Verified && status.Status != DocumentStatusEnum.Unverified)
            {
                errors.Add("Status", new[] { "Nie możesz nadac takiego statusu" });
                return new Response(false, errors);
            }
            if(status.Status == DocumentStatusEnum.Unverified && status.Reason == null)
            {
                errors.Add("Komentarz", new[] { "Wproawdz powód" });
                return new Response(false, errors);
            }
            document.VerificationTime = DateTime.Now;
            document.Status = status.Status;
            document.VerificationBy = user;
            if (status.Status == DocumentStatusEnum.Unverified)
            {
                document.UnverifiedReason = status.Reason;
            }

            try
            {
                _context.Documents.Update(document);
                _context.SaveChanges();
                _documentActivityService.AddActivity(document, user,
                    status.Status == DocumentStatusEnum.Verified
                        ? DocumentActivityTypeEnum.Verified
                        : DocumentActivityTypeEnum.Unverified);
                _notificationService.SendNotification(
                    status.Status == DocumentStatusEnum.Verified
                    ? NotificationTypeEnum.VerifiedDocument
                    : NotificationTypeEnum.UnverifiedDocument,
                    document,
                    document.CreatedBy);
                _emailSendService.SendNotificationEmail(document);
                return new Response(true, errors);
            }
            catch (Exception ex)
            {
                errors.Add("UNEXPECTED PATRONUM", new[] { ex.Message });
                return new Response(false, errors);
            }
        }

        public DocumentListResponse GetDocumentList(string orderBy, string orderType)
        {
            var errors = new Dictionary<string, string[]>();

            var user = _context.UserAccounts.FirstOrDefault(u => u.Email == _userEmail);
            if (user == null)
            {
                errors.Add("User", new[] { "Podany uzytkownik nie istanieje" });
                return new DocumentListResponse(errors);
            }

            IQueryable<Document> document = null;

            if (_jobPosition == JobPositionEnum.Student.ToString())
            {
                document = _context.Documents.Include(a => a.DocumentType)
                    .Include(a => a.FileList)
                    .Where(a => a.CreatedBy.Id == user.Id);
            }

            if (_jobPosition == JobPositionEnum.PracownikDziekanatu.ToString())
            {
                document = _context.Documents.Include(a => a.DocumentType)
                    .Include(a => a.FileList)
                    .Where(a => a.Status == DocumentStatusEnum.Awaiting ||
                                a.Status == DocumentStatusEnum.Return);
            }

            if (_jobPosition == JobPositionEnum.Dziekan.ToString())
            {
                document = _context.Documents.Include(a => a.DocumentType)
                    .Include(a => a.FileList)
                    .Where(a => a.Status == DocumentStatusEnum.Verified
                                && a.DocumentType.AcceptingUserAccount == JobPositionEnum.Dziekan);
            }
            if (_jobPosition == JobPositionEnum.Rektor.ToString())
            {
                document = _context.Documents.Include(a => a.DocumentType)
                    .Include(a => a.FileList)
                    .Where(a => a.Status == DocumentStatusEnum.Verified
                                && a.DocumentType.AcceptingUserAccount == JobPositionEnum.Rektor);
            }

            if (document == null)
            {
                errors.Add("User", new[] { "Podany uzytkownik nie moze posiadac listy dokument" });
                return new DocumentListResponse(errors);
            }

            var documentOrdered = document.OrderByDescending(a => a.Id);
            documentOrdered = documentOrdered.OrderAndSortBy(orderType, orderBy); // uzywanie pomocniczej metody
            var documentsList = documentOrdered.ToList();

            var documentsDtoList = _mapper.Map<List<Document>, List<DocumentToListReturnDto>>(documentsList);

            var documentAllReturn = new DocumentListReturnDto
            {
                Documents = documentsDtoList
            };

            return new DocumentListResponse(documentAllReturn);
        }

        public Response ConsiderationDocument(int id, EditModStatusDto status)
        {
            var errors = new Dictionary<string, string[]>();
            var user = _context.UserAccounts.FirstOrDefault(u => u.Email == _userEmail);
            if (user == null)
            {
                errors.Add("User", new[] { "Podany uzytkownik nie istanieje" });
                return new Response(false, errors);
            }
            var document = _context.Documents.Include(a => a.DocumentType).Include(a => a.CreatedBy).FirstOrDefault(d => d.Id == id);

            if (document == null)
            {
                errors.Add("Dokument", new[] { "Nie ma takiego dokumentu" });
                return new Response(false, errors);
            }

            if (_jobPosition != document.DocumentType.AcceptingUserAccount.ToString())
            {
                errors.Add("JobPosition", new[] { "Nie masz akich uprawnien" });
                return new Response(false, errors);
            }

            if (document.Status != DocumentStatusEnum.Verified)
            {
                errors.Add("Status", new[] { "Juz jest inny status niz Verified" });
                return new Response(false, errors);
            }

            if (status.Status != DocumentStatusEnum.Cancelled && status.Status != DocumentStatusEnum.Accepted && status.Status != DocumentStatusEnum.Return)
            {
                errors.Add("Status", new[] { "Nie możesz nadac takiego statusu" });
                return new Response(false, errors);
            }
            if (status.Status == DocumentStatusEnum.Cancelled && status.Reason == null)
            {
                errors.Add("Komentarz", new[] { "Wproawdz powód" });
                return new Response(false, errors);
            }

            document.ConsiderationTime = DateTime.Now;
            document.Status = status.Status;
            document.ConsiderationBy = user;
            if (status.Status == DocumentStatusEnum.Cancelled)
            {
                document.CancelledReason = status.Reason;
            }
            else if (status.Status == DocumentStatusEnum.Return)
            {
                document.ReturnedReason = status.Reason;
            }

            try
            {
                _context.Documents.Update(document);
                _context.SaveChanges();

                // logi
                DocumentActivityTypeEnum activityType;
                NotificationTypeEnum notificationType;
                switch (status.Status)
                {
                    case DocumentStatusEnum.Cancelled:
                        activityType = DocumentActivityTypeEnum.Cancelled;
                        notificationType = NotificationTypeEnum.CancelledDocument;
                        break;
                    case DocumentStatusEnum.Return:
                        activityType = DocumentActivityTypeEnum.Return;
                        notificationType = NotificationTypeEnum.ReturnedDocument;
                        break;
                    default:
                        activityType = DocumentActivityTypeEnum.Accepted;
                        notificationType = NotificationTypeEnum.AcceptedDocument;
                        break;
                }

                _documentActivityService.AddActivity(document, user, activityType);
                _notificationService.SendNotification(notificationType, document, document.CreatedBy);
                _emailSendService.SendNotificationEmail(document);

                return new Response(true, errors);
            }
            catch (Exception ex)
            {
                errors.Add("UNEXPECTED PATRONUM", new[] { ex.Message });
                return new Response(false, errors);
            }
        }

        public DocumentResponse EditDocument(int id, EditDocumentDto EditDocuemtModel)
        {
            var errors = new Dictionary<string, string[]>();
            var user = _context.UserAccounts.FirstOrDefault(u => u.Email == _userEmail);
            if (user == null)
            {
                errors.Add("User", new[] { "Podany uzytkownik nie istanieje" });
                return new DocumentResponse(errors);
            }
            var document = _context.Documents.FirstOrDefault(d => d.Id == id && d.CreatedBy.Id == user.Id);
            

            if (document == null)
            {
                errors.Add("Dokument", new[] { "Nie ma takiego dokumentu" });
                return new DocumentResponse(errors);
            }

            if (document.Status.ToString() == DocumentStatusEnum.Unverified.ToString() || document.Status.ToString() == DocumentStatusEnum.Awaiting.ToString())
            {
                var uploadedFileModels = new List<Files>();
                if (EditDocuemtModel.FileList != null)
                {
                    try
                    {
                        uploadedFileModels = _fileService.UploadFileToServer(EditDocuemtModel.FileList, document);
                    }
                    catch (Exception ex)
                    {
                        errors.Add("File", new[] { ex.Message });
                        return new DocumentResponse(errors);
                    }
                }

                var FileRemoveList = new List<Files>();
                if (EditDocuemtModel.RemoveFile != null)
                {
                    foreach (var fileName in EditDocuemtModel.RemoveFile)
                    {
                        var file = _context.Files.FirstOrDefault(i => i.FileName == fileName && i.Document.Id == id);

                        if (file != null) FileRemoveList.Add(file);
                    }
                }
                var ExistFile = _context.Files.Where(f => f.Document.Id == id).ToList();
                if (ExistFile.Count == FileRemoveList.Count && uploadedFileModels.Count == 0)
                {
                    errors.Add("DOkument", new[] { "Musisz wyslac co najmniej jeden dokument" });
                    return new DocumentResponse(errors);
                }
                var documentType = _context.DocumentTypes.SingleOrDefault(doc => doc.Id == EditDocuemtModel.DocumentTypeId);
                document.Title = EditDocuemtModel.Title;
                document.DocumentType = documentType;
                foreach (var file in uploadedFileModels)
                {
                    document.FileList.Add(file);
                }
                //document.FileList = uploadedFileModels;
                document.Status = DocumentStatusEnum.Awaiting;
                document.UnverifiedReason = null;
                document.VerificationBy = null;
                document.VerificationTime = null;
                foreach (var file in FileRemoveList)
                {
                    document.FileList.Remove(file);
                }
                try
                {
                    if (EditDocuemtModel.RemoveFile != null)
                        _fileService.RemoveFiles(EditDocuemtModel.RemoveFile);
                    _context.Files.RemoveRange(FileRemoveList);
                    _context.Files.AddRange(uploadedFileModels);
                    _context.Documents.Update(document);
                    _context.SaveChanges();

                    _documentActivityService.AddActivity(document, user, DocumentActivityTypeEnum.Edited);


                    var DocumentReturn =
                        _mapper.Map<Document, DocumentReturnDto>(document);
                    DocumentReturn.DocumentId = id;
                    return new DocumentResponse(DocumentReturn);
                }
                catch (Exception ex)
                {
                    errors.Add("UNEXPECTED PATRONUM", new[] { ex.Message });
                    return new DocumentResponse(errors);
                }
            }
            errors.Add("Dokument", new[] { "Nie mozesz edytowac dokumentu" });
            return new DocumentResponse(errors);
        }

    }
}
    

