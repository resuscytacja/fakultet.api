﻿using System;
using fakultet.common;
using fakultet.core.Services.Interfaces;
using fakultet.DAL;
using fakultet.DAL.Models;

namespace fakultet.core.Services
{
    public class DocumentActivityService : IDocumentActivityService
    {
        private readonly AppDbContext _context;

        public DocumentActivityService(AppDbContext context)
        {
            _context = context;
        }

        public void AddActivity(Document document, UserAccount initiatedBy, DocumentActivityTypeEnum type)
        {
            string message = "";

            switch (type)
            {
                case DocumentActivityTypeEnum.Created:
                    message =
                        $"Student {initiatedBy.FirstName} {initiatedBy.LastName} utworzył dokument {document.Title}";
                    break;
                case DocumentActivityTypeEnum.Verified:
                    message =
                        $"{initiatedBy.JobPosition.ToString()} {initiatedBy.FirstName} {initiatedBy.LastName} zmienił Status dokumentu na Zweryfikowany.";
                    break;
                case DocumentActivityTypeEnum.Unverified:
                    message =
                        $"{initiatedBy.JobPosition.ToString()} {initiatedBy.FirstName} {initiatedBy.LastName} zmienił Status dokumentu na Niezweryfikowany. Powód: {document.UnverifiedReason}";
                    break;
                case DocumentActivityTypeEnum.Accepted:
                    message =
                        $"{initiatedBy.JobPosition.ToString()} {initiatedBy.FirstName} {initiatedBy.LastName} rozpatrzył twój dokument pozytywnie i nadał mu status Zaakceptowany";
                    break;
                case DocumentActivityTypeEnum.Cancelled:
                    message =
                        $"{initiatedBy.JobPosition.ToString()} {initiatedBy.FirstName} {initiatedBy.LastName} rozpatrzył twój dokument negatywnie i nadał mu status Niezaakceptowany. Powód: {document.CancelledReason}";
                    break;
                case DocumentActivityTypeEnum.Return:
                    message =
                        $"{initiatedBy.JobPosition.ToString()} {initiatedBy.FirstName} {initiatedBy.LastName} przesłał twój dokument do ponownej weryfikacji. Powód: {document.ReturnedReason}";
                    break;
                case DocumentActivityTypeEnum.Edited:
                    message =
                        $"{initiatedBy.JobPosition.ToString()} {initiatedBy.FirstName} {initiatedBy.LastName} dokonal edycji swojego dokumentu.";
                    break;
                default:
                    message = "test";
                    break;
            }

            var activity = new DocumentActivity
            {
                Type = type,
                Document = document,
                Message = message,
                InitiatedBy = initiatedBy,
                DateTime = DateTime.Now
            };

            try
            {
                _context.DocumentActivities.Add(activity);
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
