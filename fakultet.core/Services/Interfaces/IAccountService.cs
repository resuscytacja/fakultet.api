﻿using System.Threading.Tasks;
using fakultet.core.DTO.Requested;
using fakultet.core.Responses;

namespace fakultet.core.Services.Interfaces
{
    public interface IAccountService
    {
        Task<AccountResponse> LoginStudentAsync(LoginStudentDto model);
        Task<AccountResponse> EditAccountPassword(EditAccountPasswordDto model);
    }
}
