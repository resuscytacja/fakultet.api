﻿using fakultet.common;
using fakultet.DAL.Models;

namespace fakultet.core.Services.Interfaces
{
    public interface IDocumentActivityService
    {
        void AddActivity(Document document, UserAccount initiatedBy, DocumentActivityTypeEnum type);
    }
}
