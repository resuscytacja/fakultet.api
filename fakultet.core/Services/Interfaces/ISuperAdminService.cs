﻿using System.Threading.Tasks;
using fakultet.core.DTO.Requested;
using fakultet.core.Responses;

namespace fakultet.core.Services.Interfaces
{
    public interface ISuperAdminService
    {
        Task<Response> AddAccountAsync(AddAccountDto model);
        Task<AccountResponse> LoginSuperAdminAsync(LoginSuperAdminDto model);
    }
}
