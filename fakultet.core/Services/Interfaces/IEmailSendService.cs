﻿using fakultet.common;
using fakultet.core.DTO.Requested;
using fakultet.core.Responses;
using fakultet.DAL.Models;

namespace fakultet.core.Services.Interfaces
{
    public interface IEmailSendService
    {
        void SendRegistrationEmail(string email, string password, string FirstName, string LastName);
        Response SendEmailToUnregistered(SendEmailToUnregisteredDto model);
        void SendNotificationEmail(Document document);
    }
}
