﻿using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using fakultet.DAL.Models;

namespace fakultet.core.Services.Interfaces
{
    public interface IFileService
    {
        List<Files> UploadFileToServer(IEnumerable<IFormFile> file, Document relatedDocument);
        void RemoveFiles(IEnumerable<Files> fileToRemove);
        void RemoveFile(string fileName);
        void RemoveFile(Files fileToRemove);
        void RemoveFiles(IEnumerable<string> fileToRemove);

    }
}
