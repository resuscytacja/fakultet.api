﻿using fakultet.core.DTO.Requested;
using fakultet.core.Responses;

namespace fakultet.core.Services.Interfaces
{
    public interface IDocumentService
    {
        DocumentResponse AddDocument(SaveDocumentDto documentSaveDto);
        DocumentTypeListResponse GetAllDocumentTypes();
        Response AddDocumentType(SaveDocumentTypeDto model);
        DocumentResponse GetDetailsDocument(int id);
        Response VerificationDocument(int id, EditModStatusDto status);
        Response ConsiderationDocument(int id, EditModStatusDto status);
        DocumentListResponse GetDocumentList(string orderBy, string orderType);
        DocumentResponse EditDocument(int id, EditDocumentDto EditDocuemtModel);
    }
}

