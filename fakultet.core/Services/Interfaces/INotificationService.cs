﻿using fakultet.common;
using fakultet.core.Responses;
using fakultet.DAL.Models;

namespace fakultet.core.Services.Interfaces
{
    public interface INotificationService
    {
        void SendNotification(NotificationTypeEnum type, Document document, UserAccount recipient);
        NotificationListResponse GetNotificationsByUser();
        Response MarkSingleNotificationAsRead(int notificationId);
        Response MarkAllNotificationsAsRead();
    }
}
