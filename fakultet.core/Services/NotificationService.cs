﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using AutoMapper;
using fakultet.common;
using fakultet.core.DTO.Returned;
using fakultet.core.Responses;
using fakultet.core.Services.Interfaces;
using fakultet.DAL;
using fakultet.DAL.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;

namespace fakultet.core.Services
{
    public class NotificationService : INotificationService
    {
        private readonly AppDbContext _context;
        private readonly IMapper _mapper;
        private readonly string _userEmail;

        public NotificationService(AppDbContext context, IMapper mapper, IHttpContextAccessor httpContext)
        {
            _context = context;
            _mapper = mapper;
            _userEmail = httpContext.HttpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier)
                ?.Value;
        }


        public void SendNotification(NotificationTypeEnum type, Document document, UserAccount recipient)
        {
            Notification notification = new Notification
            {
                DocumentId = document.Id, Read = false, Recipient = recipient, Type = type
            };

            switch (type)
            {
                case NotificationTypeEnum.UnverifiedDocument:
                    notification.Content = $"Status dokumentu {document.Title} zmieniono na Niezweryfikowany";
                    notification.SecondaryContent = document.DocumentType.Name;
                    break;
                case NotificationTypeEnum.VerifiedDocument:
                    notification.Content = $"Status dokumentu {document.Title} zmieniono na Zweryfikowany";
                    notification.SecondaryContent = document.DocumentType.Name;
                    break;
                case NotificationTypeEnum.CancelledDocument:
                    notification.Content = $"Dokument {document.Title} rozpatrzono negatywnie.";
                    notification.SecondaryContent = document.DocumentType.Name;
                    break;
                case NotificationTypeEnum.AcceptedDocument:
                    notification.Content = $"Dokument {document.Title} rozpatrzono pozytywnie.";
                    notification.SecondaryContent = document.DocumentType.Name;
                    break;
                case NotificationTypeEnum.ReturnedDocument:
                    notification.Content = $"Dokument {document.Title} wrócił od Administratora do ponownej weryfikacji przez Moderatora.";
                    notification.SecondaryContent = document.DocumentType.Name;
                    break;
            }

            try
            {
                _context.Notifications.Add(notification);
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public NotificationListResponse GetNotificationsByUser()
        {
            var errors = new Dictionary<string, string[]>();

            var user = _context.UserAccounts.FirstOrDefault(u => u.Email == _userEmail);
            if (user == null)
            {
                errors.Add("User", new[] { "Podane konto nie istnieje" });
                return new NotificationListResponse(errors);
            }

            var notifications = _context.Notifications
                .Include(n => n.Recipient)
                .Where(n => n.Recipient == user)
                .OrderByDescending(n => n.NotificationId)
                .ToList();

            var notificationsDto = _mapper.Map<List<Notification>, List<NotificationReturnDto>>(notifications);

            return new NotificationListResponse(notificationsDto);
        }

        public Response MarkSingleNotificationAsRead(int notificationId)
        {
            var errors = new Dictionary<string, string[]>();

            var user = _context.UserAccounts.SingleOrDefault(u => u.Email == _userEmail);
            if (user == null)
            {
                errors.Add("User", new[] { "Podane konto nie istnieje" });
                return new Response(false, errors);
            }

            var notification = _context.Notifications.Include(n => n.Recipient).SingleOrDefault(n => n.NotificationId == notificationId);
            if (notification == null)
            {
                errors.Add("Notification", new[] { "Powiadomienie o takim Id nie istnieje" });
                return new Response(false, errors);
            }
            if (notification.Read)
            {
                errors.Add("Notification", new[] { "Powiadomienie o podanym Id jest juz przeczytane" });
                return new Response(false, errors);
            }
            if (notification.Recipient.Id != user.Id)
            {
                errors.Add("Notification", new[] { "To powiadomienie nie jest przypisane do tego uzytkownika" });
                return new Response(false, errors);
            }

            notification.Read = true;

            try
            {
                _context.Notifications.Update(notification);
                _context.SaveChanges();

                return new Response(true, errors);
            }
            catch (Exception ex)
            {
                errors.Add("UNEXPECTED PATRONUM", new[] { ex.Message });
                return new Response(false, errors);
            }
        }

        public Response MarkAllNotificationsAsRead()
        {
            var errors = new Dictionary<string, string[]>();

            var user = _context.UserAccounts.SingleOrDefault(u => u.Email == _userEmail);
            if (user == null)
            {
                errors.Add("User", new[] { "Podane konto nie istnieje" });
                return new Response(false, errors);
            }

            var notifications = _context.Notifications.Where(n => n.Recipient.Id == user.Id && !n.Read).ToList();
            if (notifications.Count == 0)
            {
                errors.Add("Notification", new[] { "Brak nieodczytanych powiadomień" });
                return new Response(false, errors);
            }

            foreach (var notification in notifications)
            {
                notification.Read = true;
            }

            try
            {
                _context.Notifications.UpdateRange(notifications);
                _context.SaveChanges();

                return new Response(true, errors);
            }
            catch (Exception ex)
            {
                errors.Add("UNEXPECTED PATRONUM", new[] { ex.Message });
                return new Response(false, errors);
            }
        }
    }
}
