﻿using fakultet.core.Services.Interfaces;
using fakultet.DAL.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace fakultet.core.Services
{
    public class FileService : IFileService
    {
        public List<Files> UploadFileToServer(IEnumerable<IFormFile> files, Document relatedDocument)
        {
            var uploadedFilesModels = new List<Files>();

            if (files == null)
                return uploadedFilesModels;

            var imagesFolderPath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\file");

            if (!Directory.Exists(imagesFolderPath))
                throw new Exception("Nie znaleziono folderu wwwroot/file na serwerze.");

            foreach (var file in files)
            {
                //var fileExtension = "." + file.FileName.Split(".")[file.FileName.Split(".").Length - 1];
                var fileName = file.FileName;
                var filePath = Path.Combine(imagesFolderPath, fileName);

                try
                {
                    using (var bits = new FileStream(filePath, FileMode.Create))
                    {
                        file.CopyTo(bits);
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception($"Problem przy dodawaniu pliku {fileName}. {ex.Message}");
                }

                var newFile = new Files { FileName = fileName, Document = relatedDocument };
                uploadedFilesModels.Add(newFile);
            }

            return uploadedFilesModels;
        }

        public void RemoveFiles(IEnumerable<Files> filesToRemove)
        {
            foreach (var file in filesToRemove) RemoveFile(file);
        }

        public void RemoveFiles(IEnumerable<string> filesToRemove)
        {
            foreach (var file in filesToRemove) RemoveFile(file);
        }

        public void RemoveFile(Files fileToRemove)
        {
            RemoveFile(fileToRemove.FileName);
        }

        public void RemoveFile(string fileName)
        {
            var filePath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\file", fileName);

            try
            {
                if (File.Exists(filePath))
                    File.Delete(filePath);
                else
                    throw new Exception($"Nie znaleziono pliku o nazwie {fileName}");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
