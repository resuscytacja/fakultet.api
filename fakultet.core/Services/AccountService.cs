﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using fakultet.core.DTO.Requested;
using fakultet.core.DTO.Returned;
using fakultet.core.Responses;
using fakultet.core.Services.Interfaces;
using fakultet.DAL;
using fakultet.DAL.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace fakultet.core.Services
{
    public class AccountService : IAccountService
    {
        private readonly IConfiguration _configuration;
        private readonly SignInManager<UserAccount> _signInManager;
        private readonly UserManager<UserAccount> _userManager;
        private readonly AppDbContext _context;
        private readonly string _userEmail;

        public AccountService(SignInManager<UserAccount> signInManager,
            UserManager<UserAccount> userManager,
            IConfiguration configuration,
            AppDbContext context,
            IHttpContextAccessor httpContext)
        {
            _signInManager = signInManager;
            _userManager = userManager;
            _configuration = configuration;
            _context = context;
            _userEmail = httpContext.HttpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier)?.Value;
        }


        public async Task<AccountResponse> LoginStudentAsync(LoginStudentDto model)
        {
            var errors = new Dictionary<string, string[]>();
            var result = await _signInManager.PasswordSignInAsync(model.Email, model.Password, false, false);
            if (!result.Succeeded)
            {
                errors.Add("Konto", new[] { "Nie udało się zalogować" });
                return new AccountResponse(errors);
            }

            var appUser = _userManager.Users.SingleOrDefault(r => r.Email == model.Email);
            var userRoles = await _userManager.GetRolesAsync(appUser);

            var response = new JwtTokenReturnDto()
            {
                Token = Token.GenerateJwtToken(appUser, userRoles, _configuration)
            };

            return new AccountResponse(response);
        }

        public async Task<AccountResponse> EditAccountPassword(EditAccountPasswordDto model)
        {
            var errors = new Dictionary<string, string[]>();

            var user = _context.UserAccounts.AsNoTracking().FirstOrDefault(u => u.Email == _userEmail);
            if(user == null)
            {
                errors.Add("User", new[] { "Podane konto nie istnieje" });
                return new AccountResponse(errors);
            }

            var result = await _signInManager.CheckPasswordSignInAsync(user, model.OldPassword, false);
            if(!result.Succeeded)
            {
                errors.Add("Password", new[] { "Podeles zle haslo" });
                return new AccountResponse(errors);
            }

            var editResult = await _userManager.ChangePasswordAsync(await _userManager.FindByIdAsync(user.Id), 
                                                                    model.OldPassword, 
                                                                    model.NewPassword);
            if(!editResult.Succeeded)
            {
                errors.Add("Password", new[] { editResult.ToString() });
                return new AccountResponse(errors);
            }

            var userRoles = await _userManager.GetRolesAsync(user);


            var response = new JwtTokenReturnDto
            {
                Token = Token.GenerateJwtToken(user, userRoles, _configuration)
            };

            return new AccountResponse(response);
        }
    }
}
