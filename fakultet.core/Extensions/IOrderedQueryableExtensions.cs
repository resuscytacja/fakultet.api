﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using fakultet.core.DTO.Returned;
using fakultet.DAL.Models;

namespace fakultet.core.Extensions
{
    public static class IOrderedQueryableExtensions
    {
        public static IOrderedQueryable<Document> OrderAndSortBy(
            this IOrderedQueryable<Document> queryOrdered, string orderType, string orderBy)
        {
            orderBy = orderBy.ToLower();
            orderType = orderType.ToLower();

            if (orderType == "asc")
            {
                if (orderBy == nameof(DocumentToListReturnDto.Title).ToLower())
                    queryOrdered = queryOrdered.OrderBy(d => d.Title);
                else if (orderBy == nameof(DocumentToListReturnDto.DocumentTypeName).ToLower())
                    queryOrdered = queryOrdered.OrderBy(d => d.DocumentType.Name);
                else if (orderBy == nameof(DocumentToListReturnDto.Status).ToLower())
                    queryOrdered = queryOrdered.OrderBy(d => d.Status);
                else if (orderBy == nameof(DocumentToListReturnDto.SendTime).ToLower())
                    queryOrdered = queryOrdered.OrderBy(d => d.SendTime);

            }
            else if (orderType == "desc")
            {
                if (orderBy == nameof(DocumentToListReturnDto.Title).ToLower())
                    queryOrdered = queryOrdered.OrderByDescending(d => d.Title);
                else if (orderBy == nameof(DocumentToListReturnDto.DocumentTypeName).ToLower())
                    queryOrdered = queryOrdered.OrderByDescending(d => d.DocumentType.Name);
                else if (orderBy == nameof(DocumentToListReturnDto.Status).ToLower())
                    queryOrdered = queryOrdered.OrderByDescending(d => d.Status);
                else if (orderBy == nameof(DocumentToListReturnDto.SendTime).ToLower())
                    queryOrdered = queryOrdered.OrderByDescending(d => d.SendTime);
            }

            return queryOrdered;
        }
    }
}
