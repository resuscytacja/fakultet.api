﻿using fakultet.core.DTO.Requested;
using FluentValidation;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace fakultet.core.DTO.Validators
{
    public class SendEmailToUnregisteredDtoValidator : AbstractValidator<SendEmailToUnregisteredDto>
    {
        public SendEmailToUnregisteredDtoValidator()
        {
            RuleFor(x => x.Title)
                .NotEmpty()
                .WithMessage("Tytul wiadomosci nie moze byc pusty");
            RuleFor(x => x.EmailRecipient)
                .NotEmpty()
                .WithMessage("Email odbiorcy nie moze byc pusty")
                .EmailAddress()
                .WithMessage("Pole nie jest prawidlowym emailem");
            RuleFor(x => x.Content)
                .NotEmpty()
                .WithMessage("Tresc wiadomosci nie moze byc pusta");
            RuleForEach(x => x.FileList)
                .Must(BeValidFormat)
                .WithMessage("Przeslano plik o niepoprawnym formacie. Dostepne formaty to: pdf, doc, docx, odt");
        }

        private bool BeValidFormat(IFormFile file)
        {
            List<string> validFileFormats = new List<string>() { "pdf", "doc", "docx", "odt" };

            string fileFormat = file.FileName.Split(".")[file.FileName.Split(".").Length - 1];

            return validFileFormats.Contains(fileFormat);
        }

    }
}
