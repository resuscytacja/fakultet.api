﻿using fakultet.core.DTO.Requested;
using FluentValidation;

namespace fakultet.core.DTO.Validators
{
    public class SaveDocumentTypeDtoValidator : AbstractValidator<SaveDocumentTypeDto>
    {
        public SaveDocumentTypeDtoValidator()
        {
            RuleFor(x => x.Name)
                .NotNull()
                .WithMessage("Pole Name nie może byc puste");
            RuleFor(x => x.AcceptingUserAccount)
                .NotNull()
                .WithMessage("Pole AcceptingUserAccount nie może byc puste")
                .IsInEnum()
                // TODO: to nie działa, zanim ma czas zadziałać to MVC zwraca własny błąd o błędnej konwersji JobPosition do enum, jeśli jest błędny
                .WithMessage("not in enum");
            RuleFor(x => x.VerifyingUserAccount)
                .NotNull()
                .WithMessage("Pole VerifyingUserAccount nie może byc puste")
                .IsInEnum()
                // TODO: to nie działa, zanim ma czas zadziałać to MVC zwraca własny błąd o błędnej konwersji JobPosition do enum, jeśli jest błędny
                .WithMessage("not in enum");
            RuleFor(x => x.AllowedToSendUserAccount)
                .NotNull()
                .WithMessage("Pole AllowedToSendUserAccount nie może byc puste")
                .IsInEnum()
                // TODO: to nie działa, zanim ma czas zadziałać to MVC zwraca własny błąd o błędnej konwersji JobPosition do enum, jeśli jest błędny
                .WithMessage("not in enum");
        }
    }
}
