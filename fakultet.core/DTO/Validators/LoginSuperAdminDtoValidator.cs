﻿using fakultet.core.DTO.Requested;
using FluentValidation;

namespace fakultet.core.DTO.Validators
{
    public class LoginSuperAdminDtoValidator : AbstractValidator<LoginSuperAdminDto>
    {
        public LoginSuperAdminDtoValidator()
        {
            RuleFor(x => x.Username)
                .NotEmpty()
                .WithMessage("Pole Username nie może byc puste");

            RuleFor(x => x.Password)
                .NotEmpty()
                .WithMessage("Pole Hasło nie może byc puste");
        }
    }
}
