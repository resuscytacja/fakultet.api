﻿using fakultet.core.DTO.Requested;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace fakultet.core.DTO.Validators
{
    public class EditAccountPasswordDtoValidator : AbstractValidator<EditAccountPasswordDto>
    {
        public EditAccountPasswordDtoValidator()
        {
            RuleFor(x => x.OldPassword)
                .NotEmpty()
                .WithMessage("Pole stare haslo nie moze byc puste");
            RuleFor(x => x.NewPassword)
                .NotEmpty()
                .WithMessage("Pole nowe haslo nie moze byc puste")
                .NotEqual(x => x.OldPassword)
                .WithMessage("Nowe haslo musi byc inne niz stare");
            RuleFor(x => x.ConfirmNewPassword)
                .NotEmpty()
                .WithMessage("Pole powtorz haslo nie moze byc puste")
                .Equal(x => x.NewPassword)
                .WithMessage("Podane hasla nie zgadzaja sie");
        }
    }
}
