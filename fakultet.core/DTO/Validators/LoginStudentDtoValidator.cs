﻿using fakultet.core.DTO.Requested;
using FluentValidation;

namespace fakultet.core.DTO.Validators
{
    public class LoginStudentDtoValidator : AbstractValidator<LoginStudentDto>
    {
        public LoginStudentDtoValidator()
        {
            RuleFor(x => x.Email)
                .NotEmpty()
                .WithMessage("Pole Email nie może byc puste")
                .EmailAddress()
                .WithMessage("Pole Email nie jest prawidłowym emailem");

            RuleFor(x => x.Password)
                .NotEmpty()
                .WithMessage("Pole Haslo nie może byc puste");
        }
    }
}
