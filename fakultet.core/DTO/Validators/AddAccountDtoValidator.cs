﻿using System.Collections.Generic;
using fakultet.core.DTO.Requested;
using FluentValidation;

namespace fakultet.core.DTO.Validators
{
    public class AddAccountDtoValidator : AbstractValidator<AddAccountDto>
    {
        public AddAccountDtoValidator()
        {
            RuleFor(x => x.Email)
                .NotEmpty()
                .WithMessage("Pole Email nie może byc puste")
                .EmailAddress()
                .WithMessage("Pole Email nie jest prawidłowym emailem");

            RuleFor(x => x.FirstName)
                .NotEmpty()
                .WithMessage("Pole Imie nie może byc puste")
                .MaximumLength(20)
                .WithMessage("Pole Imie może zawierać od 1 do 20 znaków");

            RuleFor(x => x.LastName)
                .NotEmpty()
                .WithMessage("Pole Nazwisko nie może byc puste")
                .MaximumLength(30)
                .WithMessage("Pole Imie może zawierać od 1 do 30 znaków");

            RuleFor(x => x.Role)
                .NotNull()
                .WithMessage("Pole Role nie może byc puste")
                .Must(BeValidRole)
                .WithMessage("Nieprawidłowa rola. Dostępne role to: Admin, Moderator, Student.");

            RuleFor(x => x.NumerIndeksu)
                .NotNull()
                .WithMessage("Pole Numer Indeksu nie może byc puste")
                .When(x => x.Role.ToLower() == "student")
                .Length(6)
                .WithMessage("Poprawny Numer Indeksu musi mieć 6 znaków")
                .When(x => x.Role.ToLower() == "student")
                .Matches("[0-9]{6}")
                .WithMessage("Poprawny Numer Indeksu musi składać się tylko z cyfr")
                .When(x => x.Role.ToLower() == "student");

            RuleFor(x => x.JobPosition)
                .NotNull()
                .WithMessage("Pole JobPosition nie może byc puste")
                .IsInEnum() 
                // TODO: to nie działa, zanim ma czas zadziałać to MVC zwraca własny błąd o błędnej konwersji JobPosition do enum, jeśli jest błędny
                .WithMessage("not in enum");
        }

        private bool BeValidRole(string role)
        {
            List<string> validRoles = new List<string> { "admin", "moderator", "student" };

            return validRoles.Contains(role.ToLower());
        }
    }
}
