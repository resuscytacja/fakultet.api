﻿using fakultet.core.DTO.Requested;
using FluentValidation;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;

namespace fakultet.core.DTO.Validators
{
    public class SaveDocumentDtoValidator : AbstractValidator<SaveDocumentDto>
    {
        public SaveDocumentDtoValidator()
        {
            RuleFor(x => x.DocumentTypeId)
                .NotEmpty()
                .WithMessage("Pole typ dokumentu nie moze byc puste");

            RuleFor(x => x.Title)
                .NotEmpty()
                .WithMessage("Pole tytul nie moze byc puste");

            RuleForEach(x => x.FileList)
                .Must(BeValidFormat)
                .WithMessage("Przeslano plik o niepoprawnym formacie. Dostepne formaty to: pdf, doc, docx, odt");

        }

        private bool BeValidFormat(IFormFile file)
        {
            List<string> validFileFormats = new List<string>() { "pdf", "doc", "docx", "odt" };

            string fileFormat = file.FileName.Split(".")[file.FileName.Split(".").Length - 1];

            return validFileFormats.Contains(fileFormat);
        }
    }
}
