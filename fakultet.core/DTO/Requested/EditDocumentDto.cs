﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace fakultet.core.DTO.Requested
{
    public class EditDocumentDto
    {
        public int DocumentTypeId { get; set; }
        public string Title { get; set; }
        public IEnumerable<IFormFile> FileList { get; set; }
        public List<string> RemoveFile { get; set; }
    }
}
