﻿using System;
using System.Collections.Generic;
using System.Text;

namespace fakultet.core.DTO.Requested
{
    public class EditAccountPasswordDto
    {
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }
        public string ConfirmNewPassword { get; set; }
    }
}
