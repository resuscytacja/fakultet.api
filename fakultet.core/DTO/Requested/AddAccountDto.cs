﻿using fakultet.common;

namespace fakultet.core.DTO.Requested
{
    public class AddAccountDto
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Role { get; set; }
        public string NumerIndeksu { get; set; }
        public JobPositionEnum? JobPosition { get; set; }
    }
}
