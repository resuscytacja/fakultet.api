﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace fakultet.core.DTO.Requested
{
    public class SendEmailToUnregisteredDto
    {
        public string Title { get; set; }
        public string EmailRecipient { get; set; }
        public string Content { get; set; }
        public IEnumerable<IFormFile> FileList { get; set; }
    }
}
