﻿using fakultet.common;

namespace fakultet.core.DTO.Requested
{
    public class EditModStatusDto
    {
        public DocumentStatusEnum Status { get; set; }
        public string Reason { get; set; } // powód odrzucenia aby nie tworzyc powodu ale Cancelled Return i Unverified oddzielnych DTO albo pól
    }
}
