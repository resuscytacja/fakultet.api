﻿using fakultet.common;
using fakultet.DAL.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;

namespace fakultet.core.DTO.Requested
{
    public class SaveDocumentDto
    {
        public int DocumentTypeId { get; set; }
        public string Title { get; set; }
        public IEnumerable<IFormFile> FileList { get; set; }
    }
}
