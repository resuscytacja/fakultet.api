﻿namespace fakultet.core.DTO.Requested
{
    public class LoginStudentDto
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
