﻿namespace fakultet.core.DTO.Requested
{
    public class LoginSuperAdminDto
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
