﻿using fakultet.common;

namespace fakultet.core.DTO.Requested
{
    public class SaveDocumentTypeDto
    {
        public string Name { get; set; }
        public JobPositionEnum? AllowedToSendUserAccount { get; set; }
        public JobPositionEnum? AcceptingUserAccount { get; set; }
        public JobPositionEnum? VerifyingUserAccount { get; set; }
    }
}