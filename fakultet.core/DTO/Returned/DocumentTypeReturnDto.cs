﻿namespace fakultet.core.DTO.Returned
{
    public class DocumentTypeReturnDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string AllowedToSendAccountJobPosition { get; set; }
        public string AcceptingUserAccountJobPosition { get; set; }
        public string VerifyingUserAccountJobPosition { get; set; }
    }
}
