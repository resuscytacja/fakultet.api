﻿using System.Collections.Generic;

namespace fakultet.core.DTO.Returned
{
    public class DocumentTypeListReturnDto
    {
        public List<DocumentTypeReturnDto> DocumentTypeList { get; set; }
    }
}
