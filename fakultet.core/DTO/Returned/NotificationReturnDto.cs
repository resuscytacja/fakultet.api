﻿namespace fakultet.core.DTO.Returned
{
    public class NotificationReturnDto
    {
        public int NotificationId { get; set; }
        public int DocumentId { get; set; }
        public string Content { get; set; }
        public string SecondaryContent { get; set; }
        public string Type { get; set; }
        public bool Read { get; set; }
    }
}
