﻿using System.Collections.Generic;

namespace fakultet.core.DTO.Returned
{
    public class DocumentListReturnDto
    {
        public List<DocumentToListReturnDto> Documents { get; set; }
    }
}
