﻿using System;
using System.Collections.Generic;
using System.Text;

namespace fakultet.core.DTO.Returned
{
    public class DocumentToListReturnDto
    {
        public int DocumentId { get; set; }
        public string DocumentTypeName { get; set; }
        public string Status { get; set; }
        public string Title { get; set; }
        public DateTime SendTime { get; set; }
        public List<string> FileList { get; set; }
    }
}
