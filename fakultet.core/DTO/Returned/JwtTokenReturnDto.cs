﻿namespace fakultet.core.DTO.Returned
{
    public class JwtTokenReturnDto
    {
        public string Token { get; set; }
    }
}
