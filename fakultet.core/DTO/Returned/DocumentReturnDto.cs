﻿using System;
using System.Collections.Generic;

namespace fakultet.core.DTO.Returned
{
    public class DocumentReturnDto
    {
        public int DocumentId { get; set; }
        public string DocumentTypeName { get; set; }
        public int DocumentTypeId { get; set; }
        public string DocumentRecipient { get; set; }
        public string Status { get; set; }
        public string Title { get; set; }
        public DateTime SendTime { get; set; }
        public List<string> FileList { get; set; }
        public List<DocumentActivityReturnDto> DocumentActivitiesList { get; set; }
        public string CreatedBy { get; set; }
    }
}