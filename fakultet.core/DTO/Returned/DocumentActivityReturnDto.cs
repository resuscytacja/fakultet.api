﻿using System;

namespace fakultet.core.DTO.Returned
{
    public class DocumentActivityReturnDto
    {
        public string Type { get; set; }

        public string Message { get; set; }

        public string InitiatedBy { get; set; }

        public DateTime DateTime { get; set; }
    }
}