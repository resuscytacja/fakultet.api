﻿using System;
using System.Linq;
using AutoMapper;
using fakultet.common;
using fakultet.core.DTO.Requested;
using fakultet.core.DTO.Returned;
using fakultet.DAL.Models;

namespace fakultet.core.AutoMapper
{
    public class ResourceToModelProfile : Profile
    {
        public ResourceToModelProfile()
        {
            CreateMap<SaveDocumentTypeDto, DocumentType>();
            CreateMap<DocumentActivity, DocumentActivityReturnDto>()
                .ForMember(dest => dest.Type, opt => opt.MapFrom(src => Enum.GetName(typeof(DocumentActivityTypeEnum), src.Type)))
                .ForMember(dest => dest.InitiatedBy, opt => opt.MapFrom(src => $"{src.InitiatedBy.FirstName} {src.InitiatedBy.LastName}"));
            CreateMap<DocumentType, DocumentTypeReturnDto>()
                .ForMember(dest => dest.AllowedToSendAccountJobPosition, opt => opt.MapFrom(src => Enum.GetName(typeof(JobPositionEnum), src.AllowedToSendUserAccount)))
                .ForMember(dest => dest.AcceptingUserAccountJobPosition, opt => opt.MapFrom(src => Enum.GetName(typeof(JobPositionEnum), src.AcceptingUserAccount)))
                .ForMember(dest => dest.VerifyingUserAccountJobPosition, opt => opt.MapFrom(src => Enum.GetName(typeof(JobPositionEnum), src.VerifyingUserAccount)));
            CreateMap<AddAccountDto, UserAccount>()
                .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.Email))
                .ForMember(dest => dest.NumerIndeksu, opt => opt.MapFrom(src => src.Role == "student" ? src.NumerIndeksu : null));
            CreateMap<SaveDocumentDto, Document>()
                .ForMember(x => x.FileList, opt => opt.Ignore());
            CreateMap<Document, DocumentReturnDto>()
                .ForMember(dest => dest.DocumentId, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.DocumentRecipient, opt => opt.MapFrom(src => src.DocumentType.AcceptingUserAccount))
                .ForMember(dest => dest.DocumentTypeName, opt => opt.MapFrom(src => src.DocumentType.Name))
                .ForMember(dest => dest.DocumentTypeId, opt => opt.MapFrom(src => src.DocumentType.Id))
                .ForMember(dest => dest.FileList,
                    opt => opt.MapFrom(src => src.FileList.Select(file => file.FileName).ToList()))
                .ForMember(dest => dest.DocumentTypeName, opt => opt.MapFrom(src => src.DocumentType.Name))
                .ForMember(dest => dest.DocumentActivitiesList, opt => opt.MapFrom(src => src.Activities));
            CreateMap<Document, DocumentToListReturnDto>()
                .ForMember(dest => dest.DocumentId, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.DocumentTypeName, opt => opt.MapFrom(src => src.DocumentType.Name))
                .ForMember(dest => dest.FileList,
                    opt => opt.MapFrom(src => src.FileList.Select(image => image.FileName).ToList()));
            CreateMap<Notification, NotificationReturnDto>()
                .ForMember(dest => dest.Type, opt => opt.MapFrom(src => src.Type));
        }
    }
}
