﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace fakultet.common
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum DocumentStatusEnum
    {
        Awaiting,
        Verified,
        Unverified,
        Accepted,
        Cancelled,
        Return
    }
}
