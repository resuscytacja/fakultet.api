﻿namespace fakultet.common
{
    public enum DocumentActivityTypeEnum
    {
        Created,
        Verified,
        Unverified,
        Accepted,
        Cancelled,
        Return,
        Edited,
    }
}
