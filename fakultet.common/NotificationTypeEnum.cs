﻿namespace fakultet.common
{
    public enum NotificationTypeEnum
    {
        VerifiedDocument,
        UnverifiedDocument,
        AcceptedDocument,
        CancelledDocument,
        ReturnedDocument
    }
}
