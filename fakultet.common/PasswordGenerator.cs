﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace fakultet.common
{
    public static class PasswordGenerator
    {
        // rozwiązanie z https://stackoverflow.com/questions/1344221/how-can-i-generate-random-alphanumeric-strings
        public static string GetPassword(int size)
        {
            char[] chars =
                "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!@#$%^&*".ToCharArray();
            byte[] data = new byte[size];
            using (RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider())
            {
                crypto.GetBytes(data);
            }
            StringBuilder result = new StringBuilder(size);
            foreach (byte b in data)
            {
                result.Append(chars[b % (chars.Length)]);
            }
            // zapewnia przynajmniej jedną cyfrę (wymóg ustawiony w Identity Framework)
            Random rand = new Random();
            result.Append(rand.Next(10).ToString());
            return result.ToString();
        }
    }
}
