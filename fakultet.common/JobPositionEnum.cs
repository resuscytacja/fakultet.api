﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace fakultet.common
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum JobPositionEnum
    {
        Student, // pozycja dla Studenta i Superadmin (bo nie sa pracownikami uczelni)
        PracownikDziekanatu,
        Dziekan,
        Rektor,
        Superadmin
    }
}
